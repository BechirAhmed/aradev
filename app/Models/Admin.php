<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Laratrust\Traits\LaratrustUserTrait;
use App\Models\Profile;


class Admin extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    protected $table = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'email', 'api_token', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAdminsList()
    {
        return $this->sort()->paginate(10);
    }

    public function getAdmins()
    {
        return $this->sort()->get();
    }

    public function storeAdmin($request)
    {
        $profile = new Profile();

        $this->name = $request->name;
        $this->first_name = $request->first_name;
        $this->last_name  = $request->last_name;
        $this->email = $request->email;
        $this->password = bcrypt($request->password);
        $this->api_token = bin2hex(openssl_random_pseudo_bytes(30));

        $this->save();
        $this->profile()->save($profile);
        $this->attachRoles($request->roles);
    }

    public function getAdminById($id)
    {
        return $this->findOrFail($id);
    }

    public function updateAdmin($request, $id)
    {
        $admin = $this->getAdminById($id);

        $admin->name = $request->name;
        $admin->first_name = $request->first_name;
        $admin->last_name  = $request->last_name;
        $admin->email = $request->email;

        if ($request->changePassword) {
            $admin->password = bcrypt($request->password);
        }

        $admin->syncRoles($request->roles);
        $admin->touch();
        $admin->save();
    }

    /**
     * User Profile Relationships.
     *
     * @var array
     */
     public function profile()
     {
       return $this->hasOne('App\Models\Profile');
     }

     public function profiles()
      {
          return $this->belongsToMany('App\Models\Profile')->withTimestamps();
      }

    public function hasProfile($name)
    {
        foreach ($this->profiles as $profile) {
            if ($profile->name == $name) {
                return true;
            }
        }

        return false;
    }

    public function assignProfile($profile)
    {
        return $this->profiles()->attach($profile);
    }

    public function removeProfile($profile)
    {
        return $this->profiles()->detach($profile);
    }

    public function destroyAdmin($id)
    {
        $admin = $this->getAdminById($id);

        $importantRelations = $admin->posts()->count() || $admin->comments()->count();

        if (!$importantRelations) {
            $admin->detachPermissions();
            $admin->detachRoles();
            $admin->comments()->delete();
            $admin->posts()->delete();
            $admin->delete();
        }
    }

    public function scopeSort($query)
    {
        $query->latest('id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token, $this->getEmailForPasswordReset()));
    }

    public function getCreatedAtAttribute($value)
    {
        return date('j, m, Y', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('j, m, Y', strtotime($value));
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'user');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'user_id');
    }
}
