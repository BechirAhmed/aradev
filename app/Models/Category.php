<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Category extends Model
{
  protected $fillable = ['slug', 'title', 'meta_title', 'meta_description', 'status'];

  public function __construct(array $attributes = [])
  {
      parent::__construct($attributes);
  }

  public function getCategoriesList()
  {
      return $this->sort2()->paginate(10);
  }

  public function getCategories()
  {
      return $this->select('categories.*', 'title', 'meta_title', 'meta_description')
                  ->admitted()
                  ->sort()
                  ->groupBy('categories.id')
                  ->get();
  }

  public function storeCategory($request)
  {
      $data = [
          'slug' => $request->slug,
          'title' => $request->input('title'),
          'meta_title' => $request->input('meta_title'),
          'meta_description' => $request->input('meta_description'),
          'status' => $request->status,
      ];

      $category = $this->create($data);
  }

  public function getCategoryById($id)
  {
      return $this->findOrfail($id);
  }

  public function getCategoryBySlug($slug)
  {
      return $this->where('slug', $slug)->first();
  }

  public function getPostsList($category)
  {
      return $category->posts()->where('status', true)->paginate(10);
  }

  public function updateCategory($request, $id)
  {
      $category = $this->findOrFail($id);

      $category->slug = $request->slug;
      $category->status = $request->status;
      $category->title = $request->input('title');
      $category->meta_title = $request->input('meta_title');
      $category->meta_description = $request->input('meta_description');

      $category->touch();
      $category->save();
  }

  public function destroyCategory($id)
  {
      $category = $this->findOrFail($id);

      $importantRelations = $category->posts()->count();

      if (!$importantRelations) {
          $category->delete();
      }
  }

  public function scopeSort($query)
  {
      $query->orderBy('title', 'asc');
  }

  public function scopeSort2($query)
  {
      $query->latest('id');
  }

  public function scopeAdmitted($query)
  {
      $query->where('status', true);
  }

  public function setSlugAttribute($value)
  {
      $this->attributes['slug'] = strtolower($value);
  }

  public function getCreatedAtattribute($value)
  {
      return date('j, m, Y | g:i:s a', strtotime($value));
  }

  public function getUpdatedAtattribute($value)
  {
      return date('j, m, Y | g:i:s a', strtotime($value));
  }

  public function posts()
  {
      return $this->belongsToMany('App\Models\Post')->where('status', '1')->orderBy('published_at', 'desc');
  }

}
