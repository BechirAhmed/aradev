<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App;
use Purifier;
use Image;
use Storage;
use Auth;
use App\Events\NewComment;

class Post extends Model
{

    protected $fillable = ['slug', 'title', 'description', 'meta_title', 'meta_description', 'image', 'status', 'published_at', 'user_id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        // $this->defaultLocale = App::getLocale();
    }

    public function getAllPosts($request = [])
    {
      return $this->join('admins as a', 'posts.user_id', '=', 'a.id')
                  ->join('category_post as cp', 'posts.id', '=', 'cp.post_id')
                  ->select('posts.*', 'title', 'description', 'meta_title', 'meta_description', 'a.name', 'cp.category_id')
                  ->where('status', 1)
                  ->filter($request)
                  ->sort()
                  ->groupBy('posts.id')
                  ->paginate(10);
    }
    public function getLastPost($request = [])
    {
      return $this->join('admins as a', 'posts.user_id', '=', 'a.id')
                  ->join('category_post as cp', 'posts.id', '=', 'cp.post_id')
                  ->select('posts.*', 'title', 'description', 'meta_title', 'meta_description', 'a.name', 'cp.category_id')
                  ->where('status', 1)
                  ->filter($request)
                  ->sort()
                  ->groupBy('posts.id')
                  ->paginate(1);
    }
    //get all posts for administration pages
    public function getPostsList($request = [])
    {
      return $this->join('admins as a', 'posts.user_id', '=', 'a.id')
                  ->join('category_post as cp', 'posts.id', '=', 'cp.post_id')
                  ->select('posts.*', 'title', 'description', 'meta_title', 'meta_description', 'a.name', 'cp.category_id')
                  ->filter($request)
                  ->sort()
                  ->groupBy('posts.id')
                  ->paginate(10);
    }

    public function getPosts()
    {
        return $this->sort()->get();
    }

    public function storePost($request)
    {
        $data = [
            'slug' => $request->slug,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'meta_title' => $request->input('meta-title'),
            'meta-description' => $request->input('meta-description'),
            'published_at' => $request->published_at,
            'status' => $request->status,
            'user_id' => $request->user()->id,
        ];

        if ($request->hasFile('image')) {
            $fileName = $this->saveFile($request->file('image'));
            $data['image'] = $fileName;
        }

        $post = $this->create($data);
        $post->categories()->attach($request->categories);
    }

    public function getPostById($id)
    {
        return $this->findOrfail($id);
    }

    public function getPostBySlug($slug)
    {
        return $this->where('slug', $slug)->first();
    }

    public function updatePost($request, $id)
    {
        $post = $this->findOrFail($id);

        $post->slug = $request->slug;
        $post->published_at = $request->published_at;
        $post->status = $request->status;

        if ($request->hasFile('image')) {
            $this->deleteFile($post->image);
            $fileName = $this->saveFile($request->file('image'));
            $post->image = $fileName;
        }

            $post->title = $request->input('title');
            $post->description = $request->input('description');
            $post->meta_title = $request->input('meta-title');
            $post->meta_description = $request->input('meta-description');


        $post->touch();
        $post->save();
        $post->categories()->sync($request->categories);
    }

    public function storeComment($request)
    {
        $post = $this->getPostById($request->post);

        if (Auth::guard('admin')->check()) {
            $user = Auth::guard('admin')->user();
        } else {
            $user = Auth::user();
        }

        $comment = [
            'comment' => $request->comment,
            'status' => true,
            'user_id' => $user->id,
            'user_type' => get_class($user)
        ];

        $comment = $post->comments()->create($comment);

        event(new NewComment($comment));
    }

    public function destroyPost($id)
    {
        $post = $this->findOrFail($id);

        $post->comments()->delete();
        $post->categories()->detach();
        $post->delete();

        if (isset($post->image)) {
            $this->deleteFile($post->image);
        }
    }

    public function scopeSort($query)
    {
        $query->latest('id');
    }

    public function scopeFilter($query, $request)
    {
        if($request) {
            if ($request->filled('title')) {
                $query->where('title', 'like', '%'.$request->title.'%');
            }
            if ($request->filled('user')) {
                $query->where('name', 'like', '%'.$request->user.'%');
            }
            if ($request->filled('status')) {
                $query->where('status', '=', $request->status);
            }
            if ($request->filled('category')) {
                $query->where('category_id', '=', $request->category);
            }
            //print_r(date('Y-m-d', strtotime($this->formatDate($request->published_at))));
            if ($request->filled('published_at')) {
                $query->where('published_at', 'like', date('Y-m-d', strtotime($this->formatDate($request->published_at))).'%');
            }
        }
    }

    public function getPublishedAtattribute($value)
    {
        return date('M j, Y | H:i:s', strtotime($value));
    }

    public function getCreatedAtattribute($value)
    {
        return date('M j, Y | H:i:s', strtotime($value));
    }

    public function getUpdatedAtattribute($value)
    {
        return date('M j, Y | H:i:s', strtotime($value));
    }

    public function getImageAttribute($value)
    {
        if (isset($value)) {
            return $value;
        } else {
            return 'no_image.jpg';
        }
    }

    public function setPublishedAtattribute($value)
    {
        $this->attributes['published_at'] = date('M j, Y | H:i:s', strtotime($this->formatDate($value)));
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = strtolower($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = Purifier::clean($value);
    }

    private function formatDate($value)
    {
        return preg_replace(['/[\|]/', '/,\s/'], ['', '.'], $value);
    }

    private function saveFile($file)
    {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $location = public_path('images/posts/' . $fileName);
        $locationSm = public_path('images/posts/small/' . $fileName);

        Image::make($file)->save($location);
        Image::make($file)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($locationSm);

        return $fileName;
    }

    private function deleteFile($file)
    {
        Storage::delete('images/posts/' . $file);
        Storage::delete('images/posts/small/' . $file);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment')->where('status', '1')->orderBy('id', 'desc');
    }
}
