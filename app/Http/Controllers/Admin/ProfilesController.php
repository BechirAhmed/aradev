<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Admin;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use View;
use File;
use Image;
use Storage;


class ProfilesController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function profile_validator(array $data)
    {
        return Validator::make($data, [
            'prof_pic'               => '',
            'phone'                  => '',
            'bio'                    => 'max:900',
            'facebook_username'      => 'max:50',
            'twitter_username'       => 'max:50',
            'google_plus_username'   => 'max:50',
        ]);
    }

    /**
     * Fetch user
     * (You can extract this to repository method).
     *
     * @param $username
     *
     * @return mixed
     */
     public function getUserByUsername($username)
     {
       return Admin::with('profile')->wherename($username)->firstOrFail();
     }

     /**
     * Display the specified resource.
     *
     * @param string $username
     *
     * @return Response
     */
    public function show($username)
    {
        try {
            $admin = $this->getUserByUsername($username);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }


        $data = [
            'admin'         => $admin,
        ];

        return view('admin.profile.show')->with($data);
    }

    /**
     * /profiles/username/edit.
     *
     * @param $username
     *
     * @return mixed
     */
    public function edit($username)
    {
        try {
            $admin = $this->getUserByUsername($username);
        } catch (ModelNotFoundException $exception) {
            $request->session()->flash('danger', __('is not your profile'));
            return view('profile.show', 'admin' -> $admin);
        }

        $data = [
            'admin'          => $admin,
        ];

        return view('admin.profile.edit')->with($data);
    }

    /**
     * Update a user's profile.
     *
     * @param $username
     *
     * @throws Laracasts\Validation\FormValidationException
     *
     * @return mixed
     */
    public function update($username, Request $request)
    {
        $admin = $this->getUserByUsername($username);

        $input = Input::only('prof_pic', 'phone', 'bio', 'facebook_username', 'twitter_username', 'google_plus_username');

        if ($request->hasFile('prof_pic')) {
            $this->deleteFile($admin->profile->prof_pic);
            $fileName = $this->upload();
            $admin->profile->prof_pic = $fileName;
        }

        $profile_validator = $this->profile_validator($request->all());

        if ($profile_validator->fails()) {
            return back()->withErrors($profile_validator)->withInput();
        }

        if ($admin->profile == null) {
            $profile = new Profile();
            $profile->fill($input);
            $admin->profile()->save($profile);
        } else {
            $admin->profile->fill($input)->save();
        }

        $admin->save();

        if ($admin->save()) {
          return redirect('admin/profile/'.$admin->name.'/edit')->with('success', 'Updated Successfull');
        } else {
          return redirect('admin/profile/'.$admin->name.'/edit')->with('danger', 'Updated Not Successfull');
        }
    }

    private function saveFile($file)
    {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $location = public_path('images/admins/profiles/' . $fileName);

        Image::make($file)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);

        return $fileName;
    }

    private function deleteFile($file)
    {
        Storage::delete('images/admins/profiles/' . $file);
    }

    /**
     * Get a validator for an incoming update user request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name'              => 'required|max:255',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserAccount(Request $request, $id)
    {
        $currentUser = \Auth::user();
        $admin = Admin::findOrFail($id);
        $emailCheck = ($request->input('email') != '') && ($request->input('email') != $admin->email);

        $validator = Validator::make($request->all(), [
            'name'                  => 'required|max:255',
            'password'              => 'required|min:6|max:20|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        $rules = [];

        if ($emailCheck) {
            $rules = [
                'email'     => 'email|max:255|unique:admins',
            ];
        }

        $validator = $this->validator($request->all(), $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $admin->name = $request->input('name');
        $admin->first_name = $request->input('first_name');
        $admin->last_name = $request->input('last_name');

        if ($emailCheck) {
            $admin->email = $request->input('email');
        }

        if ($request->input('password') != null) {
            $admin->password = bcrypt($request->input('password'));
        }

        $admin->save();

        if ($admin->save()) {
          return redirect('admin/profile/'.$admin->name.'/edit')->with('success', 'Updated Successfully!');
        } else {
          return redirect('admin/profile/'.$admin->name.'/edit')->with('danger', 'Updated Failed!');
        }

    }

    /**
     * Upload and Update user avatar.
     *
     * @param $file
     *
     * @return mixed
     */
    public function upload()
    {
        if (Input::hasFile('prof_pic')) {
            $currentUser = \Auth::user();
            $avatar = Input::file('prof_pic');
            $filename = 'avatar.'.$avatar->getClientOriginalExtension();
            $save_path = public_path('images/admins/profiles/' . $fileName);
            $path = $save_path.$filename;
            $public_path = public_path('images/admins/profiles/' . $fileName);

            // Make the user a folder and set permissions
            File::makeDirectory($save_path, $mode = 0755, true, true);

            // Save the file to the server
            Image::make($avatar)->resize(300, 300)->save($save_path.$filename);

            // Save the public image path
            $currentUser->profile->avatar = $public_path;
            $currentUser->profile->save();

            return response()->json(['path'=> $path], 200);
        } else {
            return response()->json(false, 200);
        }
    }

    /**
     * Show user avatar.
     *
     * @param $id
     * @param $image
     *
     * @return string
     */
    public function userProfileAvatar($id, $image)
    {
        return Image::make(public_path().'images/admins/profiles/' .$image)->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
