<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin;
// use App\Models\User;
use App\Models\Role;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class AdminUserController extends \App\Http\Controllers\Controller
{
    private $adminModel;
    private $roleModel;

    public function __construct(Admin $adminModel, Role $roleModel)
    {
        $this->middleware('admin');
        $this->middleware('permission:read-admins')->only(['index', 'show']);
        $this->middleware('permission:create-admins')->only(['create', 'store']);
        $this->middleware('permission:update-admins')->only(['edit', 'update']);
        $this->middleware('permission:delete-admins')->only(['delete', 'destroy']);

        $this->adminModel = $adminModel;
        // $this->userModel  = $userModel;
        $this->roleModel  = $roleModel;
    }

    public function index()
    {
        $admins = $this->adminModel->getAdminsList();

        return view('admin.admin_user.all', [
            'admins' => $admins,
        ]);
    }

    public function create()
    {
        $roles = $this->roleModel->getRoles();
        $admins = $this->adminModel->getAdmins();

        return view('admin.admin_user.create', [
            'roles' => $roles,
            'admins' => $admins,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required|min:3|max:100|unique:admins',
            'first_name'  => 'max:255',
            'last_name'   => 'max:255',
            'email'       => 'required|email|unique:admins',
            'password'    => 'required|min:6|max:60|confirmed',
            'roles'       => 'required|array',
        ]);

        $this->adminModel->storeAdmin($request);

        $request->session()->flash('success', 'تمت إضافة المسؤول بنجاح');

        return redirect()->route('admin.admins');

    }

    public function show($id)
    {
        $admins = $this->adminModel->getAdminById($id);
        $roles  = $this->roleModel->getRoles();

        return view('admin.admin_user.show', [
            'admin' => $admins,
            'roles' => $roles,
        ]);
    }

    public function edit($id)
    {
        $admins = $this->adminModel->getAdminById($id);
        $roles = $this->roleModel->getRoles();
        $currentRole = $admins->roles->pluck('id')->toArray();

        $this->authorize('modify', [Admin::class, $admins]);

        return view('admin.admin_user.edit', [
            'admin' => $admins,
            'roles' => $roles,
            'currentRole' => $currentRole,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                'min:3',
                'max:100',
                Rule::unique('admins')->ignore($id),
            ],
            'first_name' => [
                  'required',
                  'max:255',
            ],
            'last_name' => [
                  'required',
                  'max:255',
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('admins')->ignore($id),
            ],
            'password' => 'sometimes|required|min:6|max:60|confirmed',
            'roles' => 'required|array',
        ]);

        $this->adminModel->updateAdmin($request, $id);

        $request->session()->flash('success', 'تم تحديث المستخدم بنجاح');

        return redirect()->to($request->redirect_to);
    }

    public function delete($id)
    {
        $admins = $this->adminModel->getAdminById($id);

        $this->authorize('modify', [Admin::class, $admins]);

        return view('admin.admin_user.delete', [
            'admin' => $admins,
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->adminModel->destroyAdmin($id);

        $request->session()->flash('success', 'تم حذف المسؤول بنجاح');

        return redirect()->to($request->redirect_to);
    }
}
