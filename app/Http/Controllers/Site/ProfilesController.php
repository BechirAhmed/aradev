<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Admin;
use App\Models\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use View;
use File;
use Image;
use Storage;

class ProfilesController extends \App\Http\Controllers\Controller
{

  protected $postModel;

  public function __construct(Post $postModel)
  {
      $this->postModel = $postModel;
  }

  /**
   * Fetch user
   * (You can extract this to repository method).
   *
   * @param $username
   *
   * @return mixed
   */
   public function getUserByUsername($username)
   {
     return Admin::with('profile')->wherename($username)->firstOrFail();
   }

   /**
   * Display the specified resource.
   *
   * @param string $username
   *
   * @return Response
   */
  public function show($username)
  {
      try {
          $admin = $this->getUserByUsername($username);
          $posts = $this->postModel->getAllPosts();
          $lastPosts = $this->postModel->getLastPost();
      } catch (ModelNotFoundException $exception) {
          abort(404);
      }


      $data = [
          'admin'       => $admin,
          'posts'       => $posts,
          'lastPosts'   => $lastPosts,
      ];

      return view('site.author.show')->with($data);
  }

  /**
   * Show user avatar.
   *
   * @param $id
   * @param $image
   *
   * @return string
   */
  public function userProfileAvatar($id, $image)
  {
      return Image::make(public_path().'images/admins/profiles/' .$image)->response();
  }
}
