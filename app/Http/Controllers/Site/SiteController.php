<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Admin;


class SiteController extends \App\Http\Controllers\Controller
{
    protected $postModel;
    protected $categoryModel;
    protected $commenModel;

    public function __construct(Post $postModel, Category $categoryModel, Comment $commentModel)
    {
        $this->postModel = $postModel;
        $this->categoryModel = $categoryModel;
        $this->commentModel = $commentModel;
    }

    public function index()
    {
        $posts = $this->postModel->getAllPosts();
        $lastPosts = $this->postModel->getLastPost();
        $categories = $this->categoryModel->getCategories();

        return view('site.home', [
            'posts' => $posts,
            'categories' => $categories,
            'lastPosts'   => $lastPosts,
        ]);
    }

    public function showPost($slug)
    {
        $posts = $this->postModel->getAllPosts();
        $post = $this->postModel->getPostBySlug($slug);
        $lastPosts = $this->postModel->getLastPost();
        $categories = $this->categoryModel->getCategories();

        return view('site.post', [
            'post' => $post,
            'categories' => $categories,
            'lastPosts'   => $lastPosts,
        ])->withPosts($posts)->withCategories($categories)->withlastPosts($lastPosts);
    }

    public function showCategory($slug)
    {
        $category = $this->categoryModel->getCategoryBySlug($slug);
        $posts = $this->categoryModel->getPostsList($category);
        $lastPosts = $this->postModel->getLastPost();
        // $categories = $this->categoryModel->getCategories();

        return view('site.category', [
            'category' => $category,
            'posts' => $posts,
            'lastPosts'   => $lastPosts,
        ]);
    }

    public function storeComment(Request $request)
    {
        $request->validate([
            'comment' => 'max:500',
            'post' => 'integer',
        ]);

        $this->postModel->storeComment($request);
    }

    public function showComment($id)
    {
        $comment = $this->commentModel->getCommentById($id);

        return view('site.partials._comment', [
            'comment' => $comment,
        ]);
    }

    public function showContacts()
    {
        $posts = $this->postModel->getAllPosts();
        $categories = $this->categoryModel->getCategories();
        return view('site.contacts')->withPosts($posts)->withCategories($categories);
    }

    public function showAboutUs()
    {
        $posts = $this->postModel->getAllPosts();
        $categories = $this->categoryModel->getCategories();
        return view('site.about_us')->withPosts($posts)->withCategories($categories);
    }
}
