<?php

namespace App\Http\Middleware;

use Closure;
use Kaishiyoku\Menu\Facades\Menu;
use App\Models\Category;

class MakeMenu
{
    private $categoryModel;

    public function __construct(Category $categoryModel)
    {
        $this->categoryModel = $categoryModel;
    }

    public function handle($request, Closure $next)
    {

        Menu::register('admin_nav', [
            Menu::link('admin.home',  __('المدونة')),
            Menu::dropdown([
                Menu::link('admin.admins',  __('المسؤولين')),
                // Menu::link('admin.users',  __('المستخدمين')),
                Menu::dropdownDivider(),
                Menu::link('admin.roles',  __('الأدوار')),
                Menu::link('admin.permissions',  __('الأذونات')),
            ],  __('المستخدمين')),
            Menu::dropdown([
                Menu::link('admin.posts',  __('المشاركات')),
                Menu::link('admin.categories',  __('الفئات')),
                Menu::link('admin.comments',  __('التعليقات')),
            ],  __('المدونة')),
        ], ['class' => 'nav navbar-nav']);

        /////////////////////////////////////////////////////////////////////////////////////////////////

        // $categoriesArray = [];
        //
        // foreach($this->categoryModel->getCategories() as $category) {
        //     $categoriesArray[] = Menu::link('site.category.show', '<i class="fa fa-circle-o"></i>&nbsp;&nbsp;' . $category->title, ['slug' => $category->slug]);
        // }
        //
        // Menu::register('site_nav', [
        //     Menu::link('site.home', '<i class="fa fa-home"></i> ' . __('المدونة')),
        //     Menu::dropdown($categoriesArray, '<i class="fa fa-th-list"></i> ' . __('الفئات')),
        //     Menu::link('site.contacts', '<i class="fa fa-phone"></i> ' . __('اتصل بنا')),
        //     Menu::link('site.aboutus', '<i class="fa fa-info-circle"></i> ' . __('حول أراديف')),
        // ], ['class' => 'nav navbar-nav']);

        return $next($request);
    }
}
