<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Post;
use App\Models\Comment;
use Carbon\Carbon;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 5; $i++) {

            $categoryData = [
                'slug' => 'test-category-' . $i,
                'status' => '1',
                'title' => 'جديد ' . $i,
                'meta_title' => 'جديد ' . $i,
                'meta_description' => 'نص تجريبي رقم #' . $i . ' لتجربة الموقع.',

            ];

            Category::create($categoryData);
        }

        for ($i = 1; $i <= 15; $i++) {

            $postData = [
                'slug' => 'test-post-' . $i,
                'title' => 'مقال تجريبي ' . $i,
                'description' => '<p>مقال تجريبي لتجريب التطبيقمقال تجريبي لتجريب التطبيقمقال تجريبي لتجريب التطبيقمقال تجريبي لتجريب التطبيقمقال تجريبي لتجريب التطبيق.</p>',
                'meta_title' => 'مقال تجريبي جديد ' . $i,
                'meta_description' => 'مقال تجريبي جديد #' . $i . ' لتجربة التطبيق',
                'status' => '1',
                'user_id' => rand(1,3),
                'published_at' => Carbon::now(),
            ];

            $post = Post::create($postData);

            $post->categories()->attach(rand(1,5));

            for ($k = 1; $k <= 3; $k++) {

                $comment = new Comment([
                    'comment' => 'تعليق #' . $k . ' لتجربة التطبيق:"' . $post->id . '"',
                    'status' => '1',
                    'user_id' => rand(1,3),
                    'user_type' => 'App\Models\User',
                ]);

                $post->comments()->save($comment);

            }

        }

    }
}
