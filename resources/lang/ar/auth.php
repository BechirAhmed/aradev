<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'عذرا! المعلومات التي أدخلتها غير صحيحة',
    'throttle' => 'عذرا! لقد حاولت كثيرا. أعد المحاولة بعد :seconds ثواني.',

    'titles' => [
        'user_login'            => 'تسجيل دخول المستخدم',
        'admin_login'           => 'تسجيل دخول المسؤول',
        'user_register'         => 'إنشاء حساب المستخدم',
        'user_password_reset'   => 'استعادة كلمة المرور للمستخدم',
        'admin_password_reset'  => 'استعادة كلمة المرور للمسؤول',
    ],

    'labels' => [
        'name'                  => 'الإسم',
        'email'                 => 'البريد الالكتروني',
        'password'              => 'كلمة السر',
        'password_confirm'      => 'تأكيد كلمة  السر',
        'remember_me'           => 'تذكرني',
        'forgot_your_password'  => 'نسيت كلمة المرور؟',
    ],

    'buttons' => [
        'login'                 => 'تسجيل الدخول',
        'register'              => 'إنشاء حساب',
        'password_reset'        => 'استعادة كلمة المرور',
    ],

];
