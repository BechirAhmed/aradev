<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'كلمة المرور يجب أن تكون على الأقل 6 حروف (أو أرقام) ومتطابق.',
    'reset' => 'تم إعادة تعيين كلمة المرور الخاصة بك!',
    'sent' => 'تم إرسال رابط تعيين كلمة المرور الخاصة بك',
    'token' => 'هذا الرابط غير صالح',
    'user' => "لم نتمكن من إيجاد مستخدم يملك هذا البريد الالكتروني",

];
