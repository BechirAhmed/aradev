<?php
return [
    'buttons' => [
        'view' => 'عرض',
        'edit' => 'تعديل',
        'delete' => 'حذف',
        'save' => 'حفظ',
        'cancel' => 'إلغاء',
        'create' => 'إضافة',
        'search' => 'بحث',
    ],
    'menu' => [
        'main' => 'الرئيسية',
        'users' => 'المستخدمين',
        'permissions' => 'التصريحات',
        'roles' => 'الأدوار',
        'admin_users' => 'المشرفين',
        'site_users' => 'المستخدمين ',
        'blog' => 'المدونة',
        'posts' => 'المشاركات',
        'categories' => 'الفئات',
        'comments' => 'التعليقات',
    ],
    'labels' => [
        'created_at' => 'إضافة',
        'updated_at' => 'تحديث',
        'important' => 'مهمة',
    ],
    'copyright' => 'Developed by Bechir Ahmed',
];
