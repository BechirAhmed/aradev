<?php
return [
    'buttons' => [
        'read_more' => 'اقرأ المزيد',
        'send' => 'ارسال',
    ],
    'menu' => [
        'blog' => 'المدونة',
        'categories' => 'الفئات',
        'contacts' => 'اتصل بنا',
        'about_us' => 'عن المدونة',
    ],
    'copyright' => 'Developed by Bechir Ahmed',
];
