<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login'     => 'تسجيل الدخول',
    'register'  => 'إنشاء حساب',
    'logout'    => 'تسجيل الخروج',

    'errors' => [
        '403' => 'Ups! ليست لديك صلاحيات لدخول هذه الصفحة',
        '404' => 'Ups! الصفحة غير موجودة',
    ],

    'buttons' => [
        'back_to_previous_page' => 'عودة للوراء',
    ],

];
