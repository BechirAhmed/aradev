
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Slug = require('slug');
Slug.defaults.mode = "rfc3986";

import Buefy from 'buefy';

Vue.use(Buefy);

// mix.js('node_modules/moment/src/moment.js', 'public/js');
// mix.js('node_modules/moment/locale/ru.js', 'public/js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));
Vue.component('slugWidget', require('./components/slugWidget.vue'));

// $(document).ready(function(){
//   $('#tab_header ul li.item').on('click', function() {
//       var number = $(this).data('option');
//       $('#tab_header ul li.item').removeClass('is-active');
//       $(this).addClass('is-active');
//       $('#tab_container .container_item').removeClass('is-active');
//       $('div[data-item="' + number + '"]').addClass('is-active');
//     });
// });

require('./manage');
