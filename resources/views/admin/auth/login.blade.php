@extends('layouts.admin')

@section('title')
    تسجيل الدخول
@endsection

@section('top_styles')
  <style media="screen">
    body, html {
      height: 100%;
      background: url({{ asset('images/bg.jpg') }}) repeat fixed;
      width: 100%;
      background-position: center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
      -o-background-size: cover;
    }
    .input {
      background: transparent;
      color: #fff;
    }
  </style>
@endsection

@section('content')
  <div class="titre">
    <h1>مرحبا بكم في صفحة تسجيل الدخول</h1>
  </div>
  <div class="columns">
    <div class="column is-one-third col-md-offset-4 col-sm-offset-4 m-t-50">

      <div class="login-form">
        <form class="" action="{{ route('admin.login.submit') }}" method="POST" role="form">
          {{ csrf_field() }}
          <div class="field">
            <label for="email" class="label">البريد الالكتروني</label>
            <p class="control">
              <input class="input {{ $errors->has('email') ? 'is-danger' : 'is-primary' }}" type="email" name="email" id="email" value="{{ old('email') }}" placeholder="name@example.com" autofocus>
            </p>
            @if ($errors->has('email'))
              <p class="help is-danger">{{ $errors->first('email') }}</p>
            @endif
          </div>
          <div class="field">
            <label for="password" class="label">كلمة السر</label>
            <p class="control">
              <input class="input {{ $errors->has('password') ? 'is-danger' : 'is-primary' }}" type="password" name="password" id="password" >
            </p>
            @if ($errors->has('password'))
              <p class="help is-danger">{{ $errors->first('password') }}</p>
            @endif
          </div>

          <b-checkbox name="remember" class="m-t-20">&nbsp; تذكرني</b-checkbox>

          <button class="button is-primary is-fullwidth m-t-30">
            تسجيل الدخول
          </button>
        </form>
        <h5 class="has-text-centered m-t-35"><a href="{{ route('admin.password.request') }}" class="is-muted">نسيت كلمة السر؟</a></h5>
      </div>

    </div>
  </div>
@endsection

@section('bottom_scripts')
  <script>
      var app = new Vue({
        el: '#app',
        data: {}
      });
  </script>
@endsection
