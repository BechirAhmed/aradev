@extends('layouts.admin')

@section('title')
    استعادة كلمة المرور
@endsection

@section('top_styles')
  <style media="screen">
    body, html {
      height: 100%;
      background: url({{ asset('images/bg.jpg') }}) repeat fixed;
      width: 100%;
      background-position: center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
      -o-background-size: cover;
    }
    .input {
      background: transparent;
      color: #fff;
    }
    .label {
      color: #fff;
    }
  </style>
@endsection

@section('content')
  <div class="columns">
    <div class="column is-one-third col-md-offset-4 col-sm-offset-4 m-t-50">
      <div class="login-form">

        <form class="form-horizontal" method="POST" action="{{ route('admin.password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="field">
                <label for="email" class="label">البريد الالكتروني</label>

                <p class="control">
                    <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ $email or old('email') }}" autofocus>
                </p>

                    @if ($errors->has('email'))
                        <p class="help is-danger">
                          {{ $errors->first('email') }}
                        </p>
                    @endif
            </div>

            <div class="field">
                <label for="password" class="label">كلمة السر</label>

                <p class="control">
                    <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                </p>

                    @if ($errors->has('password'))
                        <p class="help is-danger">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
            </div>

            <div class="field">
                <label for="password-confirm" class="label">تأكيد كلمة السر</label>
                <p class="control">
                    <input id="password-confirm" type="password" class="input{{ $errors->has('password_confirmation') ? ' is-dangerr' : '' }}" name="password_confirmation" required>
                </p>

                    @if ($errors->has('password_confirmation'))
                        <p class="help is-danger">
                            {{ $errors->first('password_confirmation') }}
                        </p>
                    @endif
            </div>

            <div class="field text-right">
                <button type="submit" class="button is-info">
                    إعادة تعيين كلمة المرور
                </button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
