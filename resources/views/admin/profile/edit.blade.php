@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      @if ($admin->profile)
        @if (Auth::user()->id == $admin->id)
          <div class="panel panel-primary m-t-20">
            <div class="panel-heading">

  						<div class="btn-group pull-right btn-group-xs">

  							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  								<i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
  								<span class="sr-only">edit</span>
  							</button>

  							<ul class="dropdown-menu">
  								<li class="active">
  									<a data-toggle="pill" href=".edit_profile" class="profile-trigger">
  										تعديل الصفحة
  									</a>
  								</li>
  								<li>
  									<a data-toggle="pill" href=".edit_settings" class="settings-trigger">
  								    تعديل الحساب
  									</a>
  								</li>
  							</ul>
            </div>

  						<div class="tab-content">
  							<span class="tab-pane active edit_profile">
  								تعديل الصفحة
  							</span>
  							<span class="tab-pane edit_settings">
  								تعديل الحساب
  							</span>
  						</div>

  					</div>
            {{-- <div class="panel-body">
              <div class="col-md-4">
                <h3 class="title" style="margin:0">تعديل صفحة {{ $admin->name }}</h3>
              </div>
              <div class="col-md-8 text-right">
                <a href="{{ URL::Previous() }}" class="button is-danger">إلغاء</a>
              </div>
            </div>
          </div> --}}
        <hr m-t-0>
        @endif
      @endif
      <div class="panel panel-body">
      @if ($admin->profile)
        @if (Auth::user()->id == $admin->id)

          <div class="tab-content">
            <div class="tab-pane fade in active edit_profile">
              {{-- <div class="row">
                <div class="col-md-12">
                  <div id="avatar_container">
                    <div class="collapseOne panel-collapse collapse in">
                      <div class="panel-body">
                        <img src="{{ $admin->profile->prof_pic }}" alt="{{ $admin->name }}" class="user-avatar">
                      </div>
                    </div>
                    <div class="collapseTwo panel-collapse collapse in">
                      <div class="panel-body">

                        <div class="dz-preview"></div>

                        <form class="form single-dropzone dropzone single" action="{{ route('avatar.upload') }}" method="POST" name="avatarDropzone" id="avatarDropzone">
                          <img id="user_selected_avatar" class="user-avatar" src="@if ($admin->profile->prof_pic != NULL) {{ $admin->profile->prof_pic }} @endif" alt="{{ $admin->name }}">
                        </form>

                      </div>
                    </div>
                  </div>
                </div>
              </div> --}}

              <div class="card">
                <div class="card-content">

                  <form action="{{ route('profile.update', $admin->name) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="columns">
                      <div class="column is-one-quarter">
                        <div class="admin-profile">
                          @if ($admin->profile->prof_pic)
                            <img src="{{ asset('images/admins/profiles/'.$admin->profile->prof_pic) }}" class="pic" alt="User Picture">
                          @endif
                        </div>
                        <div class="admin-social-icon m-t-15">
                          <input type="file" name="prof_pic" id="prof_pic">
                        </div>
                      </div>
                      <div class="column">
                        <h3 class="title is-three">نبذة عن {{ $admin->name }}</h3>
                        <div class="field usr-about">
                          <textarea name="bio" class="textarea"
                          >{{ old('bio', $admin->profile->bio) }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="columns">
                      <div class="column is-one-quarter">
                        <label for="facebook_username">Facebook Username</label>
                        <input type="text" class="input" name="facebook_username" value="{{ old('facebook_username', $admin->profile->facebook_username) }}">
                      </div>
                      <div class="column is-one-quarter">
                        <label for="twitter_username">Twitter Username</label>
                        <input type="text" class="input" name="twitter_username" value="{{ old('twitter_username', $admin->profile->twitter_username) }}">
                      </div>
                      <div class="column is-one-quarter">
                        <label for="google_plus_username">Google Plus Username</label>
                        <input type="text" class="input" name="google_plus_username" value="{{ old('google_plus_username', $admin->profile->google_plus_username) }}">
                      </div>
                      <div class="column is-one-quarter">
                        <label for="phone">رقم الهاتف</label>
                        <input type="text" class="input" name="phone" value="{{ old('phone', $admin->profile->phone) }}">
                      </div>
                    </div>
                    <hr>
                    <div class="text-center">
                      <button type="sumbit" class="button is-success is-fullwidth"><i class="fa fa-save"></i>&nbsp;حفظ الاعدادات</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="tab-pane fade edit_settings">
              {!! Form::model($admin, array('action' => array('Admin\ProfilesController@updateUserAccount', $admin->id), 'method' => 'PUT')) !!}
                {{ csrf_field() }}
                <div class="columns">
                  <div class="column">

                    <div class="field">
                      <label for="name" class="label">اسم المستخدم</label>
                      <p class="control">
                        <input type="text" name="name" value="{{ old('name', $admin->name) }}" class="input">
                      </p>
                    </div>

                    <div class="field">
                      <label for="first_name" class="label">الاسم الأول</label>
                      <p class="control">
                        <input type="text" name="first_name" value="{{ old('first_name', $admin->first_name) }}" class="input">
                      </p>
                    </div>

                    <div class="field">
                      <label for="last_name" class="label">الاسم الأخير</label>
                      <p class="control">
                        <input type="text" name="last_name" value="{{ old('last_name', $admin->last_name) }}" class="input">
                      </p>
                    </div>

                    <div class="field">
                      <label for="email" class="label">البريد الالكتروني</label>
                      <p class="control">
                        <input type="text" name="email" value="{{ old('email', $admin->email) }}" class="input">
                      </p>
                    </div>

                  </div>
                  <div class="column">
                  <b-checkbox name="change_password" v-model="change_password">&nbsp;&nbsp;تغيير كلمة السر</b-checkbox>
                    <div class="field" v-if="change_password">
                      <label for="password">كلمة السر الجديدة</label>
                      <p class="control">
                        <input type="password" name="password" class="input" id="password">
                      </p>
                      <label for="password_confirmation">تأكيد كلمة السر الجديدة</label>
                      <p class="control">
                        <input type="password" name="password_confirmation" class="input" id="password_confirmation">
                      </p>
                    </div>
                    </div>
                  </div>
                  <hr>
                  <div class="text-center">
                    <button type="sumbit" class="button is-success is-fullwidth"><i class="fa fa-save"></i>&nbsp;حفظ الاعدادات</button>
                  </div>
                {!! Form::close() !!}
            </div>
            </div>

            <div class="tab-pane fade edit_account">
              <div class="columns">

              </div>
            </div>
          </div>

        @else
          <div class="columns">
            <div class="column">
              <div class="card text-center m-t-100 m-b-100">
                <div class="">
                  <i class="fa fa-warning fa-lg" style="padding-top:10%; font-size:50px; color:#E53935"></i>
                </div>
                <div class="m-t-30">
                  <h1 class="title" style="font-size:50px; color:#E53935">
                    عذرا هذه ليست صفحتك!
                  </h1>
                  <p style="padding-bottom:10%; font-size:20px; color:#E53935">
                    لايمكنك التعديل عليها
                  </p>
                </div>
              </div>
            </div>
          </div>
        @endif
      @endif
    </div>
  </div>
</div>
  </div>
  </div>
@endsection

@section('bottom_scripts')
  {{-- @include('admin.profile.user-avatar-dz') --}}
  <script type="text/javascript">

  var app = new Vue({
        el: '#app',
        data:{
          change_password: false
        }
      });

		$('.dropdown-menu li a').click(function() {
			$('.dropdown-menu li').removeClass('active');
		});

		$('.profile-trigger').click(function() {
			$('.panel').alterClass('panel-*', 'panel-default');
		});

		$('.settings-trigger').click(function() {
			$('.panel').alterClass('panel-*', 'panel-info');
		});

		$('.admin-trigger').click(function() {
			$('.panel').alterClass('panel-*', 'panel-warning');
			$('.edit_account .nav-pills li, .edit_account .tab-pane').removeClass('active');
			$('#changepw')
				.addClass('active')
				.addClass('in');
			$('.change-pw').addClass('active');
		});

		$('.warning-pill-trigger').click(function() {
			$('.panel').alterClass('panel-*', 'panel-warning');
		});

		$('.danger-pill-trigger').click(function() {
			$('.panel').alterClass('panel-*', 'panel-danger');
		});

		$('#user_basics_form').on('keyup change', 'input, select, textarea', function(){
		    $('#account_save_trigger').attr('disabled', false);
		});

		$('#checkConfirmDelete').change(function() {
		    var submitDelete = $('#delete_account_trigger');
		    var self = $(this);

		    if (self.is(':checked')) {
		        submitDelete.attr('disabled', false);
		    }
		    else {
		    	submitDelete.attr('disabled', true);
		    }
		});

		// $("#password_confirmation").keyup(function() {
		// 	checkPasswordMatch();
		// });
    //
		// $("#password, #password_confirmation").keyup(function() {
		// 	enableSubmitPWCheck();
		// });
    //
		// $('#password, #password_confirmation').hidePassword(true);
    //
		// $('#password').password({
		// 	shortPass: 'The password is too short',
		// 	badPass: 'Weak - Try combining letters & numbers',
		// 	goodPass: 'Medium - Try using special charecters',
		// 	strongPass: 'Strong password',
		// 	containsUsername: 'The password contains the username',
		// 	enterPass: false,
		// 	showPercent: false,
		// 	showText: true,
		// 	animate: true,
		// 	animateSpeed: 50,
		// 	username: false, // select the username field (selector or jQuery instance) for better password checks
		// 	usernamePartialMatch: true,
		// 	minimumLength: 6
		// });
    //
		// function checkPasswordMatch() {
		//     var password = $("#password").val();
		//     var confirmPassword = $("#password_confirmation").val();
		//     if (password != confirmPassword) {
		//         $("#pw_status").html("Passwords do not match!");
		//     }
		//     else {
		//         $("#pw_status").html("Passwords match.");
		//     }
		// }
    //
		// function enableSubmitPWCheck() {
		//     var password = $("#password").val();
		//     var confirmPassword = $("#password_confirmation").val();
		//     var submitChange = $('#pw_save_trigger');
		//     if (password != confirmPassword) {
		//        	submitChange.attr('disabled', true);
		//     }
		//     else {
		//         submitChange.attr('disabled', false);
		//     }
		// }

	</script>
@endsection
