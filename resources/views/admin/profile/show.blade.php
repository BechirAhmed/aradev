@extends('layouts.admin')

@section('title')
    صفحة المسؤول
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 m-t-20">

            <div class="panel panel-default">
              <div class="panel-body @if (!$admin->profile) is-warning @endif">

                  <div class="row">
                    <div class="col-md-4">
                      {{-- <a href="{{ route('admin.admins') }}" class="button {{ $admin->profile ? 'is-primary is-outlined' : 'is-danger'}}"> الرجوع إلى صفحة المسؤولين</a> --}}
                    </div>
                    @if (!$admin->profile)
                      <div class="col-md-8">
                        <div class="notice">
                          <p class="title">المستخدم لم يقم بإعداد صفحته بعد&nbsp;<i class="fa fa-warning"></i></p>
                        </div>
                      </div>
                    @else
                      <div class="col-md-4 text-right">
                          <h3 class="title is-three">
                            صفحة
                            @foreach ($admin->roles as $role)
                              ال{{ $role->display_name }}
                            @endforeach
                             {{ $admin->first_name }} {{ $admin->last_name }}
                          </h3>
                      </div>
                    @endif
                    @if ($admin->profile)
                      @if (Auth::user()->id == $admin->id)
                        <div class="col-md-4 text-right">
                          <a href="{{ url('admin/profile/'.$admin->name.'/edit') }}" class="button is-primary is-outlined">
                            تعديل الصفحة
                          </a>
                        </div>
                      @endif
                    @endif
                  </div>

              </div>
            </div>

          @if ($admin->profile)
            <div class="card">
              <div class="card-content">
                <div class="columns">
                  <div class="column is-one-quarter">
                    <div class="admin-profile">
                      @if ($admin->profile->prof_pic)
                        <img src="{{ asset('images/admins/profiles/'.$admin->profile->prof_pic) }}" class="pic" alt="">
                      @else
                        <img src="{{ asset('images/admins/profiles/user2-160x160.png') }}" class="pic" alt="">
                      @endif
                    </div>
                    <div class="admin-social-icon m-t-10 m-b--10">

                        <div class="icon-group">
                          @if ($admin->profile->facebook_username)
                            <a href="{{ url('https://www.facebook.com/'.$admin->profile->facebook_username, $admin->profile->facebook_username) }}" target="_blank">
                              <div class="icon icon-fb">
                                <i class="fa fa-facebook"></i>
                              </div>
                            </a>
                          @endif
                          @if ($admin->profile->twitter_username)
                            <a href="{{ url('https://www.twitter.com/'.$admin->profile->twitter_username, $admin->profile->twitter_username) }}" target="_blank" class="m-r-10 m-l-10">
                              <div class="icon icon-tw">
                                <i class="fa fa-twitter"></i>
                              </div>
                            </a>
                          @endif
                          @if ($admin->profile->google_plus_username)
                            <a href="{{ url('https://www.google-plus.com/'.$admin->profile->google_plus_username, $admin->profile->google_plus_username) }}" target="_blank">
                              <div class="icon icon-gp">
                                <i class="fa fa-google-plus"></i>
                              </div>
                            </a>
                          @endif

                        </div>

                    </div>
                  </div>
                  <div class="column">
                    <h3 class="title is-three">نبذة عن {{ $admin->name }}</h3>
                    <div class="usr-about">
                      @if ($admin->profile->bio)
                        <p>
                          {{ $admin->profile->bio }}
                        </p>
                      @endif
                    </div>
                  </div>
                </div>
                <hr class="m-t-0">

              @endif
                <div class="columns" style="background-color:#F5F8FA">
                  <div class="column">

                      <table class="table admn-table">
                        <tbody>
                          <tr>
                            <th>
                              الاسم الكامل
                            </th>
                            <td class="text-left">
                              {{ $admin->first_name }} {{ $admin->last_name }}
                            </td>
                          </tr>
                          <tr>
                            <th>
                              اسم المستخدم
                            </th>
                            <td>
                              {{ $admin->name }}
                            </td>
                          </tr>
                          <tr>
                            <th>
                              البريد الاكتروني
                            </th>
                            <td>
                              {{ $admin->email }}
                            </td>
                          </tr>
                          @if ($admin->profile)
                            <tr>
                              <th>
                                رقم الهاتف
                              </th>
                              <td dir="ltr">
                                @if ($admin->profile->phone)
                                  {{ $admin->profile->phone }}
                                @else
                                  ---
                                @endif
                              </td>
                            </tr>
                          @endif
                          <tr>
                            <th>
                              الدور
                            </th>
                            <td>
                              @foreach($admin->roles as $role)
                                  {{ $role->display_name }}
                              @endforeach
                            </td>
                          </tr>
                          <tr>
                            <th>
                              عدد المنشورات
                            </th>
                            <td>
                              {{ $admin->posts->count() }}
                            </td>
                          </tr>
                        </tbody>
                      </table>

                  </div>
                  <div class="column">
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>الأذونات :</b></div>
                        <div class="panel-body {{ $errors->has('roles') ? 'has-error' : '' }}">

                            <ul>
                            @foreach($admin->allPermissions() as $permission)
                                <li>{{ $permission->description }}</li>
                            @endforeach
                            </ul>

                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
