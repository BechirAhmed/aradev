@extends('layouts.admin')

@section('title')
    @lang('admin/user.titles.admin_user_delete') "{{ $admin->name }}" ?
@endsection


@section('content')

<div class="flex-container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                        <h3 style="margin:0">حذف المسؤول "{{ $admin->first_name }}-{{ $admin->last_name }}" ؟</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            @if($admin->allPermissions()->count() || $admin->roles()->count() || $admin->posts()->count() || $admin->comments()->count())
                                @if($admin->posts()->count() || $admin->comments()->count())
                                    <div class="alert alert-danger" role="alert">هذ المسؤول لديه علاقات مهمة، في حال تم حذفه ستفقد كل هذه العلاقات</div>
                                @else
                                    <div class="alert alert-danger" role="alert">هذ المسؤول لديه علاقات، في حال تم حذفه ستفقد كل هذه العلاقات</div>
                                @endif
                            @endif

                            <table class="table">
                                <thead>
                                    <th>اسم العلاقة</th>
                                    <th>العدد</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>الأذونات</td>
                                        <td>{{ $admin->allPermissions()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td>الأدوار</td>
                                        <td>{{ $admin->roles()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            المشاركات
                                            <span class="label label-danger">مهمة</span>
                                        </td>
                                        <td>{{ $admin->posts()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            التعليقات
                                            <span class="label label-danger">مهمة</span>
                                        </td>
                                        <td>{{ $admin->comments()->count() }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    @if(!($admin->posts()->count() || $admin->comments()->count()))
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="{{ route('admin.admin.destroy', ['id' => $admin->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="text" name="redirect_to" hidden value="{{ URL::previous() }}">
                                            <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-trash"></i> حذف</button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ URL::previous() }}" class="btn btn-default btn-block">إلغاء</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="{{ URL::previous() }}" class="btn btn-default btn-block">@lang('admin/common.buttons.cancel')</a>
                            </div>
                        </div>
                    @endif

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
