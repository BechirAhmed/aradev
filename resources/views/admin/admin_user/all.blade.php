@extends('layouts.admin')

@section('title')
    المسؤولين
@endsection


@section('content')
<div class="flex-container">
    <div class="columns">
        <div class="column">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="title is-3">المسؤولين</h3>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin.admin.create') }}" class="button is-success"><i class="fa fa-plus"></i>&nbsp; إضافة</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">اسم المستخدم</th>
                                <th class="text-center">الاسم الأول</th>
                                <th class="text-center">الاسم الأخير</th>
                                <th class="text-center">البريد الالكتروني</th>
                                <th class="text-center">الدور</th>
                                <th class="text-center">التاريخ</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach($admins as $admin)
                                <tr>
                                    <td>{{ $admin->id }}</td>
                                    <td>{{ $admin->name }}</td>
                                    <td>{{ $admin->first_name }}</td>
                                    <td>{{ $admin->last_name }}</td>
                                    <td dir="LTR">{{ $admin->email }}</td>
                                    <td>
                                        @foreach($admin->roles as $role)
                                            {{ $role->display_name }}
                                        @endforeach
                                    </td>
                                    <td dir="LTR">
                                        <i class="fa fa-plus"></i> {{ $admin->created_at }}<br>
                                        <i class="fa fa-refresh"></i> {{ $admin->updated_at }}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.admin.show', ['id' => $admin->id]) }}" class="button is-wight is-small"><i class="fa fa-eye"></i>&nbsp; مشاهدة</a>
                                        <a href="{{ route('admin.admin.edit', ['id' => $admin->id]) }}" class="button is-wight is-small"><i class="fa fa-edit"></i>&nbsp; تعديل</a>
                                        <a href="{{ route('admin.admin.delete', ['id' => $admin->id]) }}" class="button is-danger is-small"><i class="fa fa-trash"></i>&nbsp; حذف</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @if($admins->lastPage() > 1)
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        {{ $admins->links() }}
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
