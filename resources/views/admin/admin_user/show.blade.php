@extends('layouts.admin')

@section('title')
    صفحة المسؤول
@endsection

@section('content')

<div class="flex-container">
    <div class="row">
        <div class="col-md-12 m-t-20 m-b-20">

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                      <div class="col-md-8">
                        <a href="{{ URL::previous() }}" class="button is-info is-outlined"><i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp;الرجوع إلى صفحة المسؤولين</a>
                      </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ url('/admin/profile/'.$admin->name) }}" class="button is-info is-outlined">
                            معاينة  صفحة
                               {{ $admin->first_name }} {{ $admin->last_name }}
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="card">
              <div class="card-content">

                <div class="columns">
                  <div class="column">
                    <div class="panel panel-default">
                      <div class="panel-heading"><b>المعلومات :</b></div>
                      <div class="panel-body {{ $errors->has('roles') ? 'has-error' : '' }}">
                        <table class="table admn-table is-striped">
                          <tbody>
                            <tr>
                              <th>
                                الاسم الكامل
                              </th>
                              <td>
                                {{ $admin->first_name }} {{ $admin->last_name }}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                اسم المستخدم
                              </th>
                              <td>
                                {{ $admin->name }}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                البريد الاكتروني
                              </th>
                              <td>
                                {{ $admin->email }}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                الدور
                              </th>
                              <td>
                                @foreach($admin->roles as $role)
                                    {{ $role->display_name }}
                                @endforeach
                              </td>
                            </tr>
                            <tr>
                              <th>
                                عدد المنشورات
                              </th>
                              <td>
                                5
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="column">
                    <div class="panel panel-default">
                        <div class="panel-heading"><b>الأذونات :</b></div>
                        <div class="panel-body {{ $errors->has('roles') ? 'has-error' : '' }}">

                            <ul>
                            @foreach($admin->allPermissions() as $permission)
                                <li>{{ $permission->description }}</li>
                            @endforeach
                            </ul>

                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
    </div>
</div>

@endsection
