@extends('layouts.admin')

@section('title')
    تعديل المستخدم
@endsection

@section('content')

<div class="flex-container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <h3 style="margin:0">تعديل حساب المسؤول</h3>

                </div>
            </div>

            <form action="{{ route('admin.admin.update', ['id' => $admin->id]) }}" method="POST">

                <div class="row">
                    <div class="col-md-6">

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>المعلومات :</b></div>
                            <div class="panel-body">

                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="text" name="redirect_to" value="{{ old('redirect_to', URL::previous()) }}" hidden>
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="name">اسم المستخدم</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $admin->name) }}" placeholder="اسم المستخدم" >
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                    <label for="first_name">الاسم الأول</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name', $admin->first_name) }}" placeholder="الاسم الأول" >
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                    <label for="last_name">الاسم الأخير</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name', $admin->last_name) }}" placeholder="الاسم الأخير" >
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email">البريد الالكتروني</label>
                                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $admin->email) }}" placeholder="البريد الالكتروني">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body">

                                {{-- <div class="checkbox" id="changePassword">
                                    <label>
                                        <input type="checkbox" name="changePassword" v-on:click="greet" {{ old('changePassword') ? 'checked' : '' }}> تغيير كلمة السر
                                    </label>
                                </div> --}}
                                <div class="field" id="changePassword">
                                  <b-checkbox name="changePassword" v-model="changePassword" native-value="true">تغيير كلمة السر</b-checkbox>
                                </div>

                                <div id="new_password" v-if="changePassword == true">

                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">كلمة سر جديدة</label>
                                        <input id="password" type="password" class="form-control" name="password" placeholder="كلمة السر الجديدة" >

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm">تأكيد كلمة السر الجديدة</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="تأكيد كلمة السر الجديدة" >
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>الأدوار :</b></div>
                            <div class="panel-body {{ $errors->has('roles') ? 'has-error' : '' }}">
                                @foreach($roles as $role)
                                  <div class="field">
                                    <b-radio v-model="rolesSelected" name="roles[]" :native-value="{{ $role->id }}"> {{ $role->display_name }}</b-radio>
                                  </div>
                                @endforeach

                                @if ($errors->has('roles'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="button is-info is-fullwidth"><i class="fa fa-save"></i>&nbsp; حفظ</button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ old('redirect_to', URL::previous()) }}" class="button is-wight is-fullwidth"><i class="fa fa-times"></i>&nbsp; إلغاء</a>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@foreach($admin->roles as $role)
  @php
    $roleId = $role->id
  @endphp
@endforeach
@endsection

@section('bottom_scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
          rolesSelected: {{ $roleId }},
          changePassword: ''
        },
        methods: {
            // greet: function (event) {
            //     if(event.target.checked) {
            //         $('#new_password').show()
            //         $('#new_password input').prop('disabled', false)
            //     } else {
            //         $('#new_password').hide()
            //         $('#new_password input').prop('disabled', true)
            //     }
            // }
        },
    });
    app.changePassword = false
</script>
@endsection
