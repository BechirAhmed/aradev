@extends('layouts.admin')

@section('title')
    إضافة دور جديد
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <h3 class="title is-3">إضافة دور جديد</h3>

                </div>
            </div>

                    <form action="{{ route('admin.role.store') }}" method="POST">

                        <div class="row">
                            <div class="col-md-6">

                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>معلومات</b></div>
                                    <div class="panel-body">

                                        {{ csrf_field() }}
                                        <input type="text" name="redirect_to" value="{{ old('redirect_to', URL::previous()) }}" hidden>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <label class="label" for="name">عنوان الدور</label>
                                            <input type="text" class="input" id="name" name="name" value="{{ old('name') }}" placeholder="" autofocus>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('display_name') ? 'has-error' : '' }}">
                                            <label class="label" for="display_name">اسم الدور</label>
                                            <input type="text" class="input" id="display_name" name="display_name" value="{{ old('display_name') }}" placeholder="">
                                            @if ($errors->has('display_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('display_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                            <label class="label" for="description">وصف الدور</label>
                                            <input type="text" class="input" id="description" name="description" value="{{ old('description') }}" placeholder="">
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 {{ $errors->has('description') ? 'has-error' : '' }}">

                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>الأذونات:</b></div>
                                    <div class="panel-body">

                                        <div style="overflow-y:scroll;height:200px;">

                                        @foreach($permissions as $permission)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="{{ $permission->id }}" name="permissions[]"
                                                {{ is_array(old("permissions")) && in_array($permission->id, old("permissions")) ? "checked" : "" }}>
                                                {{ $permission->display_name }}
                                            </label>
                                        </div>
                                        @endforeach

                                        </div>

                                        @if ($errors->has('permissions'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('permissions') }}</strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="button is-info is-fullwidth"><i class="fa fa-plus"></i>&nbsp; إضافة</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ old('redirect_to', URL::previous()) }}" class="button is-wight is-fullwidth"><i class="fa fa-times"></i>&nbsp; إلغاء</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>



        </div>
    </div>
</div>

@endsection
