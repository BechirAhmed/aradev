@extends('layouts.admin')

@section('title')
    @lang('admin/user.titles.role_show')
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-8">
                            <h3 style="margin:0">@lang('admin/user.titles.role_show')</h3>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ URL::previous() }}" class="btn btn-default"><i class="fa fa-hand-o-left"></i> @lang('admin/user.buttons.role_back')</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-6">

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>@lang('admin/user.titles.info'):</b></div>
                        <div class="panel-body">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>@lang('admin/user.labels.name'):</th>
                                        <td class="text-right">{{ $role->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('admin/user.labels.display_name'):</th>
                                        <td class="text-right">{{ $role->display_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('admin/user.labels.description'):</th>
                                        <td class="text-right">{{ $role->description }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('admin/common.labels.created_at'):</th>
                                        <td class="text-right" dir="LTR">{{ $role->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('admin/common.labels.updated_at'):</th>
                                        <td class="text-right" dir="LTR">{{ $role->updated_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="col-md-6 {{ $errors->has('description') ? 'has-error' : '' }}">

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>@lang('admin/user.titles.permissions'):</b></div>
                        <div class="panel-body">

                            <ul>
                                @foreach($role->permissions as $permission)
                                <li>{{ $permission->description }}</li>
                                @endforeach
                            </ul>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
