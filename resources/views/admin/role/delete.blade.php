@extends('layouts.admin')

@section('title')
    مسح الدور "{{ $role->name }}" ؟
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                        <h3 style="margin:0">مسح الدور "{{ $role->display_name }}" ؟</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            @if($role->permissions()->count() || $role->admins()->count() || $role->users()->count())
                                @if($role->admins()->count() || $role->users()->count())

                                    <b-message type="is-danger">
                                      عذرا! هذ الدور له علاقات مهمة لايمكن حذفة!
                                    </b-message>
                                @else
                                    <b-message type="is-danger">تحذير وجود علاقات</b-message>
                                @endif
                            @endif

                            <table class="table">
                                <thead>
                                    <th>اسم العلاقة</th>
                                    <th>عدد العلاقات</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>الأذونات</td>
                                        <td>{{ $role->permissions()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            المسؤولين
                                            <b-tag type="is-danger">مهمة</b-tag>
                                        </td>
                                        <td>{{ $role->admins()->count() }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    @if(!($role->admins()->count() || $role->users()->count()))
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="{{ route('admin.role.destroy', ['id' => $role->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="text" name="redirect_to" hidden value="{{ URL::previous() }}">
                                            <button type="submit" class="button id-danger is-fullwidth"><i class="fa fa-trash"></i> حذف</button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ URL::previous() }}" class="button is-wight is-fullwidth">إلغاء</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="{{ URL::previous() }}" class="button is-wight is-fullwidth">إلغاء</a>
                            </div>
                        </div>
                    @endif

                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('bottom_scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {

      }
    });
    app.closable = false
  </script>
@endsection
