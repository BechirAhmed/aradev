@extends('layouts.admin')

@section('title')
    الأدوار
@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="title is-4">الأدوار</h4>
                        </div>
                        @role('superadmin')
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin.role.create') }}" class="button is-success"><i class="fa fa-plus"></i>&nbsp; إضافة دور جديد</a>
                        </div>
                        @endrole
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table is-narrow text-center">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">الدور</th>
                                <th class="text-center">إسم الدور</th>
                                <th class="text-center">عن الدور</th>
                                <th class="text-center" style="min-width:220px">التاريخ</th>
                                @role('superadmin')
                                  <th style="min-width:350px"></th>
                                @endrole
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td dir="LTR">
                                        <i class="fa fa-plus"></i> {{ $role->created_at }}<br>
                                        <i class="fa fa-refresh"></i> {{ $role->updated_at }}
                                    </td>
                                  @role('superadmin')
                                    <td class="text-right">
                                      <a href="{{ route('admin.role.edit', ['id' => $role->id]) }}" class="button is-info is-outlined is-small"><i class="fa fa-edit"></i>&nbsp;تعديل</a>
                                      <a href="{{ route('admin.role.delete', ['id' => $role->id]) }}" class="button is-danger is-small"><i class="fa fa-trash"></i>&nbsp; حذف</a>
                                    </td>
                                  @endrole
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @if($roles->lastPage() > 1)
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        {{ $roles->links() }}
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
