@extends('layouts.admin')

@section('title')
    @lang('admin/user.titles.permissions')
@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="title is-4">الأذونات</h4>
                        </div>
                        @role('superadmin')
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin.permission.create') }}" class="button is-success"><i class="fa fa-plus"></i>&nbsp; إضافة</a>
                        </div>
                        @endrole
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover text-center">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">الاذونة</th>
                                <th class="text-center">الاسم</th>
                                <th class="text-center">عن الأذونة</th>
                                <th class="text-center" style="min-width:220px">التاريخ</th>
                                @role('superadmin')
                                  <th style="min-width:250px"></th>
                                @endrole
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td>{{ $permission->id }}</td>
                                    <td>{{ $permission->name }}</td>
                                    <td>{{ $permission->display_name }}</td>
                                    <td>{{ $permission->description }}</td>
                                    <td dir="LTR">
                                        <i class="fa fa-plus"></i> {{ $permission->created_at }}<br>
                                        <i class="fa fa-refresh"></i> {{ $permission->updated_at }}
                                    </td>
                                    @role('superadmin')
                                    <td class="text-right">
                                        <a href="{{ route('admin.permission.edit', ['id' => $permission->id]) }}" class="button is-small"><i class="fa fa-edit"></i>&nbsp; تعديل</a>
                                        <a href="{{ route('admin.permission.delete', ['id' => $permission->id]) }}" class="button is-danger is-small"><i class="fa fa-trash"></i>&nbsp; حذف</a>
                                    </td>
                                    @endrole
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @if($permissions->lastPage() > 1)
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        {{ $permissions->links() }}
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
