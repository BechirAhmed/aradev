@extends('layouts.admin')

@section('title')
    حذف الأذونة "{{ $permission->name }}" ?
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                        <h3 style="margin:0">حذف "{{ $permission->display_name }}" ?</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            @if($permission->roles()->count() || $permission->admins()->count() || $permission->users()->count())
                                <div class="alert alert-danger" role="alert">تحذير وجود علاقات لهذه الأذونة</div>
                            @endif

                            <table class="table">
                                <thead>
                                    <thاسم العلاقة</th>
                                    <th>عدد العلاقات</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>الأدوار</td>
                                        <td>{{ $permission->roles()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td>المسؤولين</td>
                                        <td>{{ $permission->admins()->count() }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{ route('admin.permission.destroy', ['id' => $permission->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="text" name="redirect_to" hidden value="{{ URL::previous() }}">
                                        <button type="submit" class="button is-danger is-fullwidth"><i class="fa fa-trash"></i>&nbsp; حذف</button>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ URL::previous() }}" class="button is-wight is-fullwidth">إلغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
