@extends('layouts.admin')

@section('title')
    @lang('admin/blog.titles.category_delete') "{{ $category->title }}" ?
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                        <h3 style="margin:0">حذف "{{ $category->title }}" ?</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            @if($category->posts()->count())
                                <div class="alert alert-danger" role="alert">تحذير لوجود علاقات مهمة</div>
                            @endif

                            <table class="table">
                                <thead>
                                    <th>اسم العلاقة</th>
                                    <th>عدد العلاقات</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            مشاركات
                                            <span class="label label-warning">مهمة</span>
                                        </td>
                                        <td>{{ $category->posts()->count() }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    @if(!$category->posts()->count())
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="{{ route('admin.category.destroy', ['id' => $category->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="text" name="redirect_to" hidden value="{{ URL::previous() }}">
                                            <button type="submit" class="button is-danger is-fullwidth"><i class="fa fa-trash"></i>&nbsp; حذف</button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ URL::previous() }}" class="button is-wight is-fullwidth">إلغاء</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="{{ URL::previous() }}" class="btn btn-default btn-block">إلغاء</a>
                            </div>
                        </div>
                    @endif

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
