@extends('layouts.admin')

@section('title')
    @lang('admin/blog.titles.category_create')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <h3 style="margin:0">إنشاء</h3>

                </div>
            </div>

            <form action="{{ route('admin.category.store') }}" method="POST">

                {{ csrf_field() }}
                <input type="text" name="redirect_to" value="{{ old('redirect_to', URL::previous()) }}" hidden>

                <div class="row">
                    <div class="col-md-4">

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>معلومات</b></div>
                            <div class="panel-body">

                                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                    <label for="slug">الرابط</label>
                                    <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug') }}" placeholder="رابط الفئة" autofocus>
                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label for="status">الحالة</label><br>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default {{ old('status') ? 'active' : '' }}">
                                            <input value="1" name="status" type="radio" autocomplete="off" {{ old('status') ? 'checked' : '' }}> <i class="fa fa-check-square-o"></i> نشر
                                        </label>
                                        <label class="btn btn-default {{ old('status') ? '' : 'active' }}">
                                            <input value="0" name="status" type="radio" autocomplete="off" {{ old('status') ? '' : 'checked' }}> <i class="fa fa-ban"></i> حفظ
                                        </label>
                                    </div>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-8">

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>المحتوى</b></div>
                            <div class="panel-body">

                                <div>

                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                      <div role="tabpanel" class="tab-pane active" id="">

                                          <br>
                                          <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                              <label for="title">العنوان</label>
                                              <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" placeholder="">
                                              @if ($errors->has("title"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("title") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                                              <label for="meta_title">العنوان التعريفي</label>
                                              <input type="text" class="form-control" id="meta_title" name="meta_title" value="{{ old('meta_title') }}" placeholder="">
                                              @if ($errors->has("meta_title"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("meta_title") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                                              <label for="meta_description">الوصف التعريفي</label>
                                              <input type="text" class="form-control" id="meta_description" name="meta_description" value="{{ old('meta_description') }}" placeholder="">
                                              @if ($errors->has("meta_description"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("meta_description") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                      </div>
                                  </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="button is-primary is-outlined is-fullwidth"><i class="fa fa-plus"></i>&nbsp; إضافة</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ old('redirect_to', URL::previous()) }}" class="button is-warning is-outlined is-fullwidth"><i class="fa fa-times"></i>&nbsp; إلغاء</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>


            </form>

        </div>
    </div>
</div>

@endsection
