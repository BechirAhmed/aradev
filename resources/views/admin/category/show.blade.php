@extends('layouts.admin')

@section('title')
    @lang('admin/blog.titles.category_show')
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                      <div class="col-md-4">
                        <a href="{{ URL::previous() }}" class="button is-wight"><i class="fa fa-hand-o-right"></i>&nbsp; الرجوع إلى كل الفئات</a>
                      </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>المعلومات</b></div>
                        <div class="panel-body">

                            <dl>
                                <dt>الرابط:</dt>
                                <dd>{{ $category->slug }}</dd>

                                <dt>الحالة:</dt>
                                <dd>{{ $category->status ? __('نشر') : __('حفظ') }}</dd>

                                <dt>إضافة في :</dt>
                                <dd>{{ $category->created_at }}</dd>

                                <dt>آخر تحديث :</dt>
                                <dd>{{ $category->updated_at }}</dd>
                            </dl>

                        </div>
                    </div>

                </div>
                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>العنوان</b></div>
                        <div class="panel-body">

                            <h2 style="margin-top:5px">{{ $category->title }}</h2>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>المحتوى التعريفي</b></div>
                        <div class="panel-body">

                            <div><b>العنوان التعريفي:</b> {{ $category->meta_title }}</div>
                            <br>
                            <div><b>الوصف التعريفي:</b> {{ $category->meta_description }}</div>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

@endsection
