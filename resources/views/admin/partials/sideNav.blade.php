<div class="side-menu" id="admin-side-menu">
  <aside>
    {{-- <div class="user-panel m-r-5">
      <div class="columns">
          <div class="column is-one-third image">
             @php
               $user_img = Auth::user()->profile->prof_pic ;
             @endphp
            <img src="{{ asset('images/admins/profiles/'.$user_img) }}" class="img-circle" alt="User Image">
          </div>
          <div class="column info">
            <p>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
            <em style="font-size:10px;"><i class="fa fa-circle" style="color:#00C853;"></i> Online</em>
          </div>
        </div>
    </div> --}}
    <div class="menu m-t-20 m-r-10">
      {{-- <p class="menu-label">
        عام
      </p> --}}
      <ul class="menu-list">
        <li><a href="{{ route('admin.home') }}" class="{{ Request::is('admin') ? 'is-active' : '' }}">الرئيسية</a></li>
      </ul>

      <p class="menu-label">
        المدونة
      </p>
      <ul class="menu-list">
        <li><a href="{{ route('admin.posts') }}" class="{{ Request::is('admin/posts') || Request::is('admin/posts/*') ? 'is-active' : '' }}">المشاركات</a></li>
        <li><a href="{{ route('admin.categories') }}" class="{{ Request::is('admin/categories') || Request::is('admin/categories/*') ? 'is-active' : '' }}">الفئات</a></li>
        <li><a href="{{ route('admin.comments') }}" class="{{ Request::is('admin/comments') || Request::is('admin/comments/*') ? 'is-active' : '' }}">التعليقات</a></li>
      </ul>


      @role('superadmin')
        <p class="menu-label">
          الإعدادات
        </p>
        <ul class="menu-list">
          <li><a href="{{ route('admin.admins') }}" class="{{ Request::is('admin/admins') || Request::is('admin/admins/*') ? 'is-active' : '' }}">المستخدمين</a></li>
          <li>
            <a class="has-submenu {{ Request::is('admin/roles') || Request::is('admin/permissions') || Request::is('admin/permissions/*') || Request::is('admin/roles/*') ? 'is-active' : '' }}">الأدوار &amp; الأذونات</a>
            <ul class="submenu">
              <li><a href="{{ route('admin.roles') }}" class="{{ Request::is('admin/roles') || Request::is('admin/roles/*') ? 'is-active' : '' }}">الأدوار</a></li>
              <li><a href="{{ route('admin.permissions') }}" class="{{ Request::is('admin/permissions') || Request::is('admin/permissions/*') ? 'is-active' : '' }}">الأذونات</a></li>
            </ul>
          </li>
        </ul>
      @endrole
      @role('admin')
        <p class="menu-label">
          الإعدادات
        </p>
        <ul class="menu-list">
          <li><a href="{{ route('admin.admins') }}" class="{{ Request::is('admin/admins') || Request::is('admin/admins/*') ? 'is-active' : '' }}">المستخدمين</a></li>
          <li>
            <a class="has-submenu {{ Request::is('admin/roles') || Request::is('admin/permissions') || Request::is('admin/permissions/*') || Request::is('admin/roles/*') ? 'is-active' : '' }}">الأدوار &amp; الأذونات</a>
            <ul class="submenu">
              <li><a href="{{ route('admin.roles') }}" class="{{ Request::is('admin/roles') || Request::is('admin/roles/*') ? 'is-active' : '' }}">الأدوار</a></li>
              <li><a href="{{ route('admin.permissions') }}" class="{{ Request::is('admin/permissions') || Request::is('admin/permissions/*') ? 'is-active' : '' }}">الأذونات</a></li>
            </ul>
          </li>
        </ul>
      @endrole

      <p class="menu-label">
        روابط مباشرة
      </p>
      <ul class="menu-list">
        <li><a href="{{ route('admin.post.create') }}">مشاركة جديدة</a></li>
        <li><a href="{{ route('admin.category.create') }}">فئة جديدة</a></li>
        @role('superadmin')
          <li><a href="{{ route('admin.admin.create') }}">إضافة مسؤول جديد</a></li>
          <li><a href="{{ route('admin.role.create') }}">إضافة دور جديد</a></li>
        @endrole
      </ul>
    </div>
  </aside>
</div>
