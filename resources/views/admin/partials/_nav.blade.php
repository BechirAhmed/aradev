<!-- Navbar -->
<nav class="navbar has-shadow navbar-home is-fixed-top" role="navigation" aria-label="dropdown navigation">
    <div class="navbar-brand">
        <!-- Collapsed Hamburger -->
        <button type="button" class="button navbar-burger" data-toggle="collapse" data-target="#app-navbar-collapse" id="nav-burger-btn">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-item is-hidden-desktop" id="admin-slideout-btn">
          <span class="icon"><i class="fa fa-arrow-circle-o-right"></i></span>
        </a>
        <!-- Branding Image -->
        <div class="brand-logo">
          <a href="{{ route('admin.home') }}" class="is-center">
            <img src="{{asset('images/ARADEEV-Logo.png')}}" alt="DigiDev" class="img-responsive">
          </a>
        </div>
    </div>

    <div class="navbar-menu" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        {{-- <div class="navbar-end">
          <div class="navbar-item">
            {!! Menu::render('admin_nav') !!}
          </div>
        </div> --}}

        <!-- Right Side Of Navbar -->
        <div class="navbar-start">
          <!-- Authentication Links -->
          @guest
              <a href="{{ route('admin.login') }}" class="navbar-item home_menu_link is-tab"><i class="fa fa-sign-in"></i> تسجيل الدخول</a>
          @else
              <div class="navbar-item has-dropdown is-hoverable">
                <a href="#" class="navbar-link home_menu_link">
                  <div class="user-panel">
                    <div class="columns">
                      <div class="column info">
                        <p>{{ Auth::user()->name }}</p>
                      </div>
                      <div class="column image m-r--15">
                         @if (Auth::user()->profile)
                            <?php $user_img = Auth::user()->profile->prof_pic; ?>
                            <img src="{{ asset('images/admins/profiles/'.$user_img )}}" class="img-circle" alt="User Image">
                          @else
                           <img src="{{ asset('images/admins/profiles/user2-160x160.png') }}" class="img-circle" alt="User Image">
                         @endif
                      </div>
                    </div>
                  </div>
                </a>

                <div class="navbar-dropdown is-left">
                  <div class="profile_dropdown">
                    <div class="profile text-center" style="background:url({{ asset('images/bg.jpg') }}) repeat fixed">
                      @if (Auth::user()->profile)
                         <?php $user_img = Auth::user()->profile->prof_pic; ?>
                         <img src="{{ asset('images/admins/profiles/'.$user_img )}}" class="img-circle" alt="User Image">
                       @else
                        <img src="{{ asset('images/admins/profiles/user2-160x160.png') }}" class="img-circle" alt="User Image">
                      @endif
                      <p class="m-t-10">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                    </div>
                    <div class="dropdown-buttons m-b--20">
                      <div class="profile-btn">
                        @if (Auth::user()->profile)
                          <a href="{{ url('/admin/profile/'.Auth::user()->name) }}" class="button is-info is-outlined">صفحتي</a>
                        @endif
                      </div>
                      <div class="logout-btn">
                        <a href="{{route('admin.logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="button is-info is-outlined">
                          تسجيل الخروج
                        </a>
                      </div>
                    </div>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </div>
                </div>
              </div>
          @endguest
          {{-- <a href="{{ route('site.home') }}" class="navbar-item is-tab" target="_blank"><i class="fa fa-home"></i></a> --}}
        </div>
    </div>
</nav>
