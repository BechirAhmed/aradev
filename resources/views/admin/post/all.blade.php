@extends('layouts.admin')

@section('title')
    المشاركات
@endsection

@section('top_styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="title is-3">المشاركات</h3>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ route('admin.post.create') }}" class="button is-success"><i class="fa fa-plus"></i>&nbsp; إضافة</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="GET" action="{{ route('admin.posts') }}">
                        <div class="row">

                        <div class="col-md-2">
                            <div class="field">
                                <label class="label" for="title">العنوان: </label>
                                <input type="text" class="input" name="title" id="title" value="{{ request()->input('title') }}" placeholder="العنوان">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="field">
                                <label class="label" for="user">الكاتب: </label>
                                <input type="text" class="input" name="user" id="user" value="{{ request()->input('user') }}" placeholder="الكاتب">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="field">
                                <label class="label" for="status">الحالة: </label>
                                <select class="select is-fullwidth" name="status" id="status">
                                    <option value="">الكل</option>
                                    <option value="1" {{ request()->input('status') === '1' ? 'selected' : '' }}>منشور</option>
                                    <option value="0" {{ request()->input('status') === '0' ? 'selected' : '' }}>محفوظ</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="field">
                                <label class="label" for="category">الفئات: </label>
                                <select class="select is-fullwidth" name="category" id="category">
                                    <option value="">الكل</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ request()->input('category') == $category->id ? 'selected' : '' }}>{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="field">
                                <label class="label" for="published_at">تاريخ النشر: </label>
                                <input type="text" class="input" id="published_at" name="published_at" value="{{ request()->filled('published_at') ? request()->input('published_at') : '' }}" placeholder="تاريخ النشر" >
                            </div>
                        </div>

                        <div class="col-md-2">
                            <button type="submit" class="button is-info is-fullwidth"><i class="fa fa-search"></i>&nbsp; بحث  </button>
                            <a href="{{ route('admin.posts') }}" class="button is-wight is-fullwidth m-t-5"><i class="fa fa-ban"></i>&nbsp; إلغاء </a>
                        </div>

                        </div>

                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover is-narrow text-center">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">الصورة</th>
                                <th class="text-center">العنوان</th>
                                <th class="text-center">الفئات</th>
                                <th class="text-center">الكاتب</th>
                                <th class="text-center">الحالة</th>
                                <th class="text-center" style="min-width:220px">التاريخ(إضافة|تحديث|نشر)</th>
                                <th style="min-width:150px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>
                                        <img src="{{ asset('images/posts/small/' . $post->image) }}" width="80" height="40px">
                                    </td>
                                    <td>{{ $post->title }}</td>
                                    <td>
                                        <ul>
                                            @foreach($post->categories as $category)
                                                <li>{{ $category->title }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>{{ $post->user->first_name }} {{ $post->user->last_name }}</td>
                                    <td>{!! $post->status ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-ban text-danger"></i>' !!}</td>
                                    <?php $date_create = (String)$post->updated_at; ?>
                                    <td class="text-center" dir="LTR">
                                        <i class="fa fa-plus"></i> {{ $post->created_at }} <br>
                                        <i class="fa fa-refresh"></i> {{ $post->updated_at }}<br>
                                        <i class="fa fa-calendar-check-o"></i> {{ $post->published_at }}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.post.show', ['id' => $post->id]) }}" class="button is-small"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('admin.post.edit', ['id' => $post->id]) }}" class="button is-small"><i class="fa fa-edit"></i></a>
                                        @role('superadmin')
                                          <a href="{{ route('admin.post.delete', ['id' => $post->id]) }}" class="button is-danger is-small"><i class="fa fa-trash"></i></a>
                                        @endrole
                                        @role('admin')
                                          <a href="{{ route('admin.post.delete', ['id' => $post->id]) }}" class="button is-danger is-small"><i class="fa fa-trash"></i></a>
                                        @endrole
                                        @role('editor')
                                          <a href="{{ route('admin.post.delete', ['id' => $post->id]) }}" class="button is-danger is-small"><i class="fa fa-trash"></i></a>
                                        @endrole
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @if($posts->lastPage() > 1)
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        {{ $posts->appends(request()->input())->links() }}
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection

@section('bottom_scripts')

<script type="text/javascript" src="{{ asset('js/moment-with-locales.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<script>

$('#published_at').datetimepicker({

    format: 'D, MM, YYYY',
    dayViewHeaderFormat: 'MMMM YYYY',
    sideBySide: true,
    showClose: true,
    widgetPositioning: {
        horizontal: 'auto',
        vertical: 'bottom',
    },
    locale: '{{ Config::get("app.locale") }}',

});

</script>

@endsection
