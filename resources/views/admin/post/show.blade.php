@extends('layouts.admin')

@section('title')
    المشاركة
@endsection

@section('content')

<div class="container m-b-50">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                      <div class="col-md-4">
                        <a href="{{ URL::previous() }}" class="button is-wight"><i class="fa fa-hand-o-right"></i>&nbsp; عودة للوراء</a>
                      </div>
                      <div class="col-md-8">
                          <h3 class="title is-3">{{ $post->title }}</h3>
                      </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-4">

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>المعلومات :</b></div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>الرابط</th>
                                        <td class="text-right"><a href="{{ route('site.post.show', ['slug' => $post->slug]) }}" target="_blank">{{ route('site.post.show', ['slug' => $post->slug]) }}</a></td>
                                    </tr>
                                    <tr>
                                        <th>الحالة :</th>
                                        <td class="text-right">{{ $post->status ? 'نشر' : 'حفظ' }}</td>
                                    </tr>
                                    <tr>
                                        <th>تاريخ الإضافة :</th>
                                        <td class="text-right" dir="LTR">{{ $post->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <th>تاريخ التحديث :</th>
                                        <td class="text-right" dir="LTR">{{ $post->updated_at }}</td>
                                    </tr>
                                    <tr>
                                        <th>تاريخ النشر :</th>
                                        <td class="text-right" dir="LTR">{{ $post->published_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>الصورة :</b></div>
                        <div class="panel-body">

                            @if($post->image)
                                <img src="{{ asset('images/posts/small/' . $post->image) }}" style="width:100%">
                            @endif

                        </div>
                    </div>

                </div>
                <div class="col-md-8">

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>المحتوى :</b></div>
                        <div class="panel-body">

                            <h2 style="margin-top:5px">{{ $post->title }}</h2>
                            <br>
                            <div>{!! $post->description !!}</div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><b>البيانات التعريفي :</b></div>
                        <div class="panel-body">

                            <div><b>العنوان التعريفي :</b> {{ $post->meta_title }}</div>
                            <br>
                            <div><b>المحتوى التعريفي :</b> {{ $post->meta_description }}</div>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

@endsection
