@extends('layouts.admin')

@section('title')
    تعديل المشاركة
@endsection

@section('top_styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">

                    <h3 style="margin:0">تعديل المشاركة</h3>

                </div>
            </div>

            <form action="{{ route('admin.post.update', ['id' => $post->id]) }}" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="text" name="redirect_to" value="{{ old('redirect_to', URL::previous()) }}" hidden>

                <div class="row">
                    <div class="col-md-4">

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>المعلومات</b></div>
                            <div class="panel-body">

                                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                    <label for="slug">الرابط</label>
                                    <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug', $post->slug) }}" placeholder="" autofocus>
                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('published_at') ? 'has-error' : '' }}">
                                    <label for="published_at">نشرت في</label>
                                    <input dir="ltr" type="text" class="form-control" id="published_at" name="published_at" value="{{ old('$published_at', $post->published_at) }}" placeholder="" autofocus>
                                    @if ($errors->has('published_at'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('published_at') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('categories') ? 'has-error' : '' }}">
                                    <label for="slug">الفئات</label>
                                    <select class="js-multiple-select" name="categories[]" multiple="multiple">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                {{ (is_array(old("categories")) && in_array($category->id, old("categories")))
                                                || (in_array($category->id, $checkedCategories) && !is_array(old("categories"))) ? "selected" : "" }}
                                            >
                                                {{ $category->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('categories'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('categories') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label for="status">الحالة</label><br>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default {{ old('status', $post->status) ? 'active' : '' }}">
                                            <input value="1" name="status" type="radio" autocomplete="off" {{ old('status', $post->status) ? 'checked' : '' }}> <i class="fa fa-check-square-o"></i> نشر
                                        </label>
                                        <label class="btn btn-default {{ old('status', $post->status) ? '' : 'active' }}">
                                            <input value="0" name="status" type="radio" autocomplete="off" {{ old('status', $post->status) ? '' : 'checked' }}> <i class="fa fa-ban"></i> حفظ
                                        </label>
                                    </div>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>الصورة</b></div>
                            <div class="panel-body">

                                @if($post->image)
                                    <img src="{{ asset('images/posts/small/' . $post->image) }}" style="width:100%">
                                @endif
                                <br>
                                <div class="well {{ $errors->has('image') ? 'has-error' : '' }}">
                                    <label for="image">صورة جديدة</label>
                                    <input type="file" id="image" name="image">
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-8">

                        <div class="panel panel-default">
                            <div class="panel-heading"><b>المحتوى</b></div>
                            <div class="panel-body">

                                <div>

                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                      <div role="tabpanel" class="tab-pane active">

                                          <br>
                                          <div class="form-group {{ $errors->has('title-') ? 'has-error' : '' }}">
                                              <label for="title">العنوان</label>
                                              <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $post->title) }}" placeholder="">
                                              @if ($errors->has("title"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("title") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div class="form-group {{ $errors->has('description-') ? 'has-error' : '' }}">
                                              <label for="description">المشاركة</label>
                                              <textarea style="height:300px;" class="form-control" id="description" name="description" placeholder="">
                                                  {{ old('description', $post->description) }}
                                              </textarea>
                                              @if ($errors->has("description"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("description") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div class="form-group {{ $errors->has('meta-title-') ? 'has-error' : '' }}">
                                              <label for="meta-title">العنوان التعريفي</label>
                                              <input type="text" class="form-control" id="meta-title" name="meta-title" value="{{ old('meta-title', $post->meta_title) }}" placeholder="">
                                              @if ($errors->has("meta-title"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("meta-title") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                          <div class="form-group {{ $errors->has('meta-description-') ? 'has-error' : '' }}">
                                              <label for="meta-description">وصف المشاركة</label>
                                              <input type="text" class="form-control" id="meta-description" name="meta-description" value="{{ old('meta-description', $post->meta_description) }}" placeholder="">
                                              @if ($errors->has("meta-description"))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first("meta-description") }}</strong>
                                                  </span>
                                              @endif
                                          </div>

                                      </div>
                                  </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 m-b-50">

                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="button is-primary is-fullwidth"><i class="fa fa-save"></i>&nbsp; حفظ</button>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ old('redirect_to', URL::previous()) }}" class="button is-wight is-fullwidth"><i class="fa fa-times"></i>&nbsp; إلغاء</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>


            </form>

        </div>
    </div>
</div>

@endsection

@section('bottom_scripts')

<script type="text/javascript" src="{{ asset('js/moment-with-locales.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=prz9vfpgqdelq0k500a5dpabhcdzuvvw9yigzddpy1lj2nd9"></script>

<script>

$('#published_at').datetimepicker({

    format: 'D, MM, YYYY | hh:mm:SS a',
    dayViewHeaderFormat: 'MMMM YYYY',
    sideBySide: true,
    showClose: true,
    widgetPositioning: {
        horizontal: 'auto',
        vertical: 'bottom',
    },
    locale: 'en',

});

tinymce.init({ selector:'textarea' });

$('.js-multiple-select').select2({
    placeholder: "@lang('admin/blog.labels.choose_categories')",
    allowClear: true,
    width: '100%',
});

</script>

@endsection
