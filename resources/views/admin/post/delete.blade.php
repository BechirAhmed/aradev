@extends('layouts.admin')

@section('title')
    حذف "{{ $post->title }}" ؟
@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                        <h3 style="margin:0">حذف "{{ $post->title }}" ؟</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="alert alert-danger" role="alert">تحذير علاقات مهمة</div>

                            <table class="table">
                                <thead>
                                    <th>اسم العلاقة</th>
                                    <th>عدد العلاقات</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> الفئات</td>
                                        <td>{{ $post->categories()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <td>التعليقات</td>
                                        <td>{{ $post->comments()->count() }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{ route('admin.post.destroy', ['id' => $post->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="text" name="redirect_to" hidden value="{{ URL::previous() }}">
                                        <button type="submit" class="button is-fullwidth is-danger"><i class="fa fa-trash"></i>&nbsp; حذف</button>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ URL::previous() }}" class="button is-fullwidth is-wight">إلغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
