
@include('admin.partials._head')

@if (Request::is('admin/login') || Request::is('admin/password/*'))
  @yield('content')

  @include('admin.partials.scripts_footer')
@else

@include('admin.partials._nav')


<div class="managemeng-area">
  @include('admin.partials._alerts')
  @include('admin.partials.sideNav')

    @yield('content')

  {{-- @include('admin.partials._footer') --}}
</div>

@include('admin.partials.scripts_footer')
@endif
