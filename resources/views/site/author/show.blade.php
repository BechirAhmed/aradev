@extends('layouts.site')

@section('title')
    صفحة المسؤول
@endsection

@section('content')
<div>
  <div class="divider">

  </div>
  <div class="c5-bg-white clearfix">
    <div id="main-container" style="transform:none">
      <div class="author-main">
        <div class="author-cover-photo" style="background-image:url({{ asset('images/cover.jpg') }})"></div>
        <div class="author-img">
          @if ($admin->profile->prof_pic)
            <img src="{{ asset('images/admins/profiles/'.$admin->profile->prof_pic) }}" width="200" height="200" alt="">
          @else
            <img src="{{ asset('images/admins/profiles/user2-160x160.png') }}" width="200" height="200" alt="">
          @endif
        </div>
        <div class="author-data">
          <h3 class="title is-3">
            <a href="#">{{ $admin->first_name }} {{ $admin->last_name }}</a>
          </h3>
          <div class="clearfix"></div>
          @if ($admin->profile->bio)
            <p class="user-description">{{ $admin->profile->bio }}</p>
          @endif
          <div class="social-icons clearfix">
            <div class="icon-group">
              @if ($admin->profile->facebook_username)
                <a href="{{ url('https://www.facebook.com/'.$admin->profile->facebook_username, $admin->profile->facebook_username) }}" target="_blank">
                  <div class="icon icon-fb">
                    <i class="fa fa-facebook"></i>
                  </div>
                </a>
              @endif
              @if ($admin->profile->twitter_username)
                <a href="{{ url('https://www.twitter.com/'.$admin->profile->twitter_username, $admin->profile->twitter_username) }}" target="_blank" class="m-r-10 m-l-10">
                  <div class="icon icon-tw">
                    <i class="fa fa-twitter"></i>
                  </div>
                </a>
              @endif
              @if ($admin->profile->google_plus_username)
                <a href="{{ url('https://www.google-plus.com/'.$admin->profile->google_plus_username, $admin->profile->google_plus_username) }}" target="_blank">
                  <div class="icon icon-gp">
                    <i class="fa fa-google-plus"></i>
                  </div>
                </a>
              @endif

            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="main-container">
        <div id="content" style="transform:none">
          <div id="inner-content" class="row">
            <div class="col-md-8 col-md-offset-2">
              @if ($admin->posts)
                @foreach ($admin->posts as $post)
                  <div class="post-block-1 clearfix">
                    <article class="post-1">
                      <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                        <div class="post-1-img">
                          <img src="{{ asset('images/posts/small/'.$post->image) }}" width="380px" height="150px" alt="">
                        </div>
                        <header class="content-header">
                          <h4 class="header-title title is-4">{{ $post->title }}</h4>
                        </header>
                        <div class="content-detail">
                          {{ mb_substr(strip_tags($post->description), 0, 70) }} ...
                          <ul class="c5-meta-data">
                            <li class="c5-meta-li-data">
                              <span>
                                <i class="fa fa-calendar"></i>
                                {{ $post->created_at }} - منذ ثلاث ساعات
                              </span>
                            </li>
                          </ul>
                        </div>
                      </a>
                    </article>
                  </div>
                @endforeach
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
