<div class="panel panel-default">
    <div class="panel-heading">
        <small dir="ltr">
            {{ $comment->created_at }}&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;
            @if($comment->user instanceof App\Models\Admin)
                <b-tag type="is-danger">مسؤول</b-tag>
            @endif
            {{ $comment->user->name }}&nbsp;<i class="fa fa-user"></i>
        </small>
    </div>
    <div class="panel-body">
        {{ $comment->comment }}
    </div>
</div>
