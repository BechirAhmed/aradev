<footer>
    <div class="fluid-container">
      <div class="footer p-t-5">
        <div class="container">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
              <li class="m-l-20"><a href="#">التسجيل</a></li>
              <li class="m-l-20"><a href="#">خريطة المجلة</a></li>
              <li><a href="#">روابط</a></li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <ul class="b-social-icon pull-right">
                  <li><a href="" class="fa fa-facebook" aria-hidden="true"> </a></li>
                  <li><a href="" class="fa fa-twitter" aria-hidden="true"> </a></li>
                  <li><a href="" class="fa fa-pinterest-p" aria-hidden="true"> </a></li>
                  <li><a href="" class="fa fa-google-plus" aria-hidden="true"> </a></li>
                  <li><a href="" class="fa fa-github" aria-hidden="true"> </a></li>
              </ul>
          </div>
          <div class="col-md-12">
            <p class="m-t-50 m-b-50">أراديـف... منصّـة إعلاميـة تثقيفيــة رقميــة شامــلة، صنعتهـا عقــول وأقلام عربيــة من المُحيط إلى الخليج، تخاطبك أينمـا كنت.. تحترم عقلك، وتُقدّم لك مُنتجــاً فكــرياً تحرص أن يكــون مُفيــداً شيّقــاً مُختلفاً..</p>
          </div>
        </div>
      </div>
    </div>
  </footer>
</div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('owlcarousel/owl.carousel.min.js') }}"></script>
<script>

jQuery(window).load(function() { // makes sure the whole site is loaded
    $('#status').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(100).css({
        'overflow': 'visible'
    });
    $(".owl-carousel").owlCarousel({
      rtl:true,
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:3
          }
      }
    });
})
</script>
    @yield('bottom_scripts')

</body>
</html>
