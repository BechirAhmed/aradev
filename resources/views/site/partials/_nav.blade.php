
<!-- Navbar -->
<nav class="navbar navbar-default navbar-static-top is-fixed-top" role="navigation">
  <div class="fluid-container">
    <div class="nav-menu">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
          <ul class="nav navbar-nav navbar-left">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background:@if(Request::is('/')) #2196F3 @else #F4511E @endif">
                {{-- <i class="fa fa-bars"></i> --}}
                <i class="material-icons mi-menu md-36"></i>
                <i class="material-icons mi-clear md-36 disabled"></i>
              </a>
                <ul class="dropdown-menu" role="menu">
                  <li class="menu-container">
                    <div class="nav-dropdown">
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <ul class="menu-ul">
                              <a href="{{ route('site.home') }}">
                                <li class="li-menu" style="background:#FFCDD2">
                                  <div class="">
                                    <h5 class="title is-5">الرئيسية</h5>
                                  </div>
                                </li>
                              </a>
                              <a href="{{ route('site.home') }}">
                                <li class="li-menu" style="background:#C5CAE9">
                                  <div class="">
                                    <h5 class="title is-5">تك</h5>
                                  </div>
                                </li>
                              </a>
                              <a href="{{ route('site.home') }}">
                                <li class="li-menu" style="background:#B2DFDB">
                                  <div class="">
                                    <h5 class="title is-5">فن</h5>
                                  </div>
                                </li>
                              </a>
                              <a href="{{ route('site.home') }}">
                                <li class="li-menu" style="background:#FFE0B2">
                                  <div class="">
                                    <h5 class="title is-5">تعليم</h5>
                                  </div>
                                </li>
                              </a>
                              <a href="{{ route('site.home') }}">
                                <li class="li-menu" style="background:#BDBDBD">
                                  <div class="">
                                    <h5 class="title is-5">الأشهر</h5>
                                  </div>
                                </li>
                              </a>
                            </ul>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            @foreach ($lastPosts as $lastPost)
                              <article class="article-one" style="background:url({{ asset('images/posts/'.$lastPost->image) }})">
                                <a class="c5-url" href="{{ route('site.post.show', ['slug' => $lastPost->slug]) }}"></a>
                                <div class="content">
                                  <h3 class="title is-3">
                                    <a href="{{ route('site.post.show', ['slug' => $lastPost->slug]) }}" class="title">{{ $lastPost->title }}</a>
                                  </h3>

                                  <p class="disabled">بواسطة
                                    <a href="{{ url('/profile', $lastPost->user->name) }}" class="by-name">{{ $lastPost->user->first_name }} {{ $lastPost->user->last_name }}</a>
                                  </p>
                                </div>
                              </article>
                            @endforeach
                          </div>
                        </div>
                      </div>
                  </li>
                </ul>
            </li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <ul class="nav navbar-nav navbar-center">
            <li>
              @if (Request::is('/'))
                <a href="{{ route('site.home') }}">
                  <img src="{{ asset('images/ARADEEV-Logo.png') }}" alt="">
                </a>
              @endif
            </li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
          <ul class="nav navbar-nav navbar-right">
            <li class="search-input">
              <div class="field">
                <div class="control has-icons-left is-clearfix">
                  <input type="search" autocomplete="on" placeholder="بحث..." class="input is-medium" expanded>
                  <span class="icon is-left is-medium">
                    <a href="#">
                      <i class="fa fa-search fa-2x"></i>
                    </a>
                  </span>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>

  </div>
</nav>

@section('bottom_scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data:{

      }
    });
    $('.dropdown-toggle').click(function(event) {
      event.preventDefault();
      $('.material-icons').toggleClass('disabled');
      // $('.fa-close').toggleClass('disabled');
    })
  </script>
@endsection
