<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="RTL">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} | @yield('title')</title>
    <meta name="description" content="@yield('description')">

    <!-- Styles -->
    <link href="{{ asset('css/preloader.css') }}" rel="stylesheet">
    <link href="{{ asset('owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('owlcarousel/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/material-icons.min.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="favicon.png" type="image/x-icon">

    @yield('top_styles')
</head>
<body>
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <div id="app">
