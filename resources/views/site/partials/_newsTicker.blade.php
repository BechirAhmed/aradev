<!-- Header Top -->
<div class="header-top visible-md visible-lg">
    <div class="container">
        <div class="col-md-offset-1 row">
            <div class="latest-div pull-left">
                <span class="support-btn pull-right"> <a href="#">ﺁﺧﺮ اﻷﺧﺒﺎﺭ</a></span>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="vticker">
                    <ul class="ticker">
                        @foreach($posts as $post)
                            <li><a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">{{ $post->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="social-icon pull-right">
                    <li><a href="" class="fa fa-facebook" aria-hidden="true"> </a></li>
                    <li><a href="" class="fa fa-twitter" aria-hidden="true"> </a></li>
                    <li><a href="" class="fa fa-pinterest-p" aria-hidden="true"> </a></li>
                    <li><a href="" class="fa fa-google-plus" aria-hidden="true"> </a></li>
                    <li><a href="" class="fa fa-github" aria-hidden="true"> </a></li>
                    <li><a href="" class="fa fa-search" aria-hidden="true"> </a></li>
                </ul>
            </div>

        </div>
    </div>
</div>
@section('ticker_script')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    {{ Html::script('js/jquery.easing.min.js') }}
    {{ Html::script('js/jquery.easy-ticker.js') }}

    <script type="text/javascript">
        $(document).ready(function(){

            var dd = $('.vticker').easyTicker({
                direction: 'up',
                easing: 'easeInOutBack',
                speed: 'slow',
                interval: 5000,
                height: 'auto',
                visible: 1,
                mousePause: 1,
                controls: {
                    up: '.up',
                    down: '.down',
                    toggle: '.toggle',
                    stopText: 'Stop !!!'
                }
            }).data('easyTicker');

            cc = 1;
            $('.aa').click(function(){
                $('.vticker ul').append('<li>' + cc + ' Triangles can be made easily using CSS also without any images. This trick requires only div tags and some</li>');
                cc++;
            });

            $('.vis').click(function(){
                dd.options['visible'] = 3;

            });

            $('.visall').click(function(){
                dd.stop();
                dd.options['visible'] = 0 ;
                dd.start();
            });

        });
    </script>
@endsection
