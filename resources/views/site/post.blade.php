@extends('layouts.site')

@section('title')
    {{ $post->title }}
@endsection

@section('top_styles')
  <style media="screen">
    body {
      background:#E9EAED;
    }
  </style>
@endsection

@section('content')
<div class="fluid-container m-t-100">
  <div class="title-container clearfix">
    <div class="container">
      <div class="main-title">
        <div class="header-title">
          <header class="clearfix">
            <h1 class="title is-1 post-title">
              <a href="javascript:void">{{ $post->title }}</a>
            </h1>
            <div class="header-meta-data clearfix">
              <div class="post-breadcrumbs hidden-md hidden-sm">
                <a href="{{ route('site.home') }}">الرئسية</a>
                 »
                 <span>
                   @foreach($post->categories as $category)
                     <a href="{{ route('site.category.show', ['slug' => $category->slug]) }}">{{ $category->title }}</a>{{ !$loop->last ? ',' : ''}}
                   @endforeach
                   »
                   <strong>{{ $post->title }}</strong>
                 </span>
              </div>
              <div class="author-img">
                @if ($post->user->profile)
                  <img src="{{ asset('images/admins/profiles/'.$post->user->profile->prof_pic) }}" width="40" height="40" alt="{{ $post->user->first_name }}">
                @endif
              </div>
              <div class="author-info">
                <a href="{{ url('author/'.$post->user->name) }}">{{ $post->user->first_name }} {{ $post->user->last_name }}</a>
                ،
                <span class="cat-in">في:</span>
                @foreach($post->categories as $category)
                  <a class="cat-link" href="{{ route('site.category.show', ['slug' => $category->slug]) }}">{{ $category->title }}</a>{{ !$loop->last ? ',' : ''}}
                @endforeach
                <br>
                <span>
                  <i class="fa fa-calendar"></i>
                  {{ $post->created_at }} - منذ ثلاث ساعات
                </span>
                ،
                <span>
                   آخر تحديث: {{ $post->updated_at }}
                </span>
              </div>
            </div>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div class="article-container">
    <div class="main-content clearfix">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">
          <div class="content clearfix">
            <div class="inner-content clearfix">
              <div class="main clearfix">
                <article class="clearfix">
                  <div class="c5-thumb clearfix">
                    <img src="{{ asset('images/posts/'.$post->image) }}" alt="{{ $post->title }}" width="887" height="592">
                  </div>
                  <section class="article-content clearfix">
                    <div class="post-content">
                      {{ $post->description }}
                    </div>
                    <div class="relatedPostsSlider owl-carousel owl-rtl owl-theme">
                      @foreach ($posts as $post)
                        <div class="item">
                          <a href="#">
                            <img src="{{ asset('images/posts/small/'.$post->image) }}" alt="" height="150">
                            <span class="relatedPost-title">
                              {{ $post->title }}
                            </span>
                          </a>
                        </div>
                      @endforeach
                    </div>
                    <div class="article-disclaimer">
                      <a href="#" class="article-disclaimer-toggle">
                        <i class="fa-left fa fa-caret-left" aria-hidden="true"></i>
                        <i class="fa-left fa fa-caret-down disabled" aria-hidden="true"></i>
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        إخلاء المسؤولية
                      </a>
                    </div>
                    <div class="article-disclaimer-content disabled">
                      مقال "{{ $post->title }}" لا يعّبر بالضرورة عن رأي فريق تحرير أراجيك. يمنع منعاً باتاً نقل أي جزء من المحتوى بأي شكل كان الا بعد الحصول على موافقة خطية من الإدارة.
                    </div>
                  </section>
                  <div class="social-share-btn clearfix">
                    <div class="like-btn clearfix">
                      <span class="add-to-fav">

                        <span class="count" id="count">0</span>
                        <span class="fa fa-heart-o fa-2x" onclick='like();'></span>
                      </span>
                    </div>
                  </div>
                  <footer class="article-footer">
                    <div class="article-comments">
                      <h4 class="title is-4">شاركنا رأيك حول "{{ $post->title }}"</h4>
                      <div class="custom-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active">
                              <a href="#customComments" aria-controls="customComments" role="tab" data-toggle="tab">
                                  <i class="fa fa-comments"></i>
                              </a>
                          </li>
                        </ul>

                        <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="customComments">
                            @if(Auth::guard('admin')->check() || Auth::check())
                                <form id="text_comment" action="{{ route('site.comment.store') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <textarea name="comment" class="form-control" placeholder="اكتب تعليقك هنا..." rows="3">{{ old('comment') }}</textarea>
                                        <input type="text" name="post" value="{{ $post->id }}" hidden>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-default">إرسال</button>
                                    </div>
                                </form>
                            @else
                                <div class="alert alert-warning" role="alert"> سجل الدخول من أجل إضافة تعليق <a href="">تسجيل الدخول</a></div>
                            @endif
                            <div class="comments">
                            @forelse ($post->comments as $comment)
                                    @include('site.partials._comment')
                            @empty
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <p>لاتعليق</p>
                                    </div>
                                </div>
                            @endforelse
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </footer>
                </article>
              </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>
@endsection

@section('bottom_scripts')
    <script type="text/javascript">

        $('.article-disclaimer-toggle').click(function(event) {
          event.preventDefault();
          $(this).find('.fa-left').toggleClass('disabled');
          $('.article-disclaimer-content').toggleClass('disabled');
        });

        $('.fa-heart-o').click(function() {
          event.preventDefault();
          $(this).css('background-color:red');
        });
        $count = 0;
        function like($x) {
          $count = $count + 1;
          document.getElementById("count").innerHTML=$count;
        }

        Echo.channel('comment.{{ $post->id }}')
            .listen('NewComment', (e) => {

                var url = '{{ route("site.comment.show", ":id") }}';
                url = url.replace(':id', e.comment.id);

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: url,
                    type: 'POST',
                    cache: false,
                    datatype: 'json',
                    success: function(data) {
                        $('.comments').prepend(data);
                    },
                });

            });

            $('body').on('submit', '#text_comment', function() {

                var url = '{{ route("site.comment.store") }}';
                var form = $(this);

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: url,
                    data: form.serialize(),
                    type: 'POST',
                    cache: false,
                    datatype: 'json',
                    success: function(data) {
                        form.find('textarea').val('');
                    },
                });

                return false;

            });
    </script>
@endsection
