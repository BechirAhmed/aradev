@extends('layouts.site')

@section('title')
    الرئيسية
@endsection

@section('content')
  <div class="fluid-container">
    <div class="site-flex-container">
      <div class="row m-t-100">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <!-- @foreach ($lastPosts as $lastPost) -->
            <article class="article-one" style="background:url({{ asset('images/posts/1513697100.jpg') }})">
              <a class="c5-url" href="{{ route('site.post.show', ['slug' => $lastPost->slug]) }}"></a>
              <div class="content">
                <h3 class="title is-3">
                  <!-- <a href="{{ route('site.post.show', ['slug' => $lastPost->slug]) }}" class="title">{{ $lastPost->title }}</a> -->
                  <a href="{{ route('site.post.show', ['slug' => $lastPost->slug]) }}" class="title">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</a>
                </h3>

                <p class="hovered">بواسطة
                  <a href="{{ url('/author', $lastPost->user->name) }}" class="by-name">البشير أحمد</a>
                </p>
              </div>
            </article>
          <!-- @endforeach -->
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="row m-l-0">
            <div class="row">
              <div class="col-md-12">
                <article class="article-two" style="background:url({{ asset('images/posts/1513697100.jpg') }})">
                  <a class="c5-url" href="#"></a>
                  <div class="content">
                    <h3 class="title is-3">
                      <a href="#" class="title">عنوان المقال التجريبي</a>
                    </h3>

                    <p class="hovered">بواسطة
                      <a href="#" class="by-name">البشير</a>
                    </p>
                  </div>
                </article>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <article class="article-three" style="background:url({{ asset('images/posts/1513697100.jpg') }})">
                  <a class="c5-url" href="#"></a>
                  <div class="content">
                    <h3 class="title is-3">
                      <a href="#" class="title">عنوان المقال التجريبي</a>
                    </h3>

                    <p class="hovered">بواسطة
                      <a href="#" class="by-name">البشير</a>
                    </p>
                  </div>
                </article>
              </div>
              <div class="col-md-6 col-sm-6">
                <article class="article-three" style="background:url({{ asset('images/posts/1513697100.jpg') }})">
                  <a class="c5-url" href="#"></a>
                  <div class="content">
                    <h3 class="title is-3">
                      <a href="#" class="title">عنوان المقال التجريبي</a>
                    </h3>

                    <p class="hovered">بواسطة
                      <a href="#" class="by-name">البشير</a>
                    </p>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="flex-container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-12">
            <hr class="star-long">
            <!-- @foreach ($posts as $post) -->
              <div class="post-block-1 clearfix">
                <article class="post-1">
                  <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                    <div class="post-1-img">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" width="380px" height="150px" alt="">
                    </div>
                    <header class="content-header">
                      <!-- <h4 class="header-title title is-4">{{ $post->title }}</h4> -->
                      <h4 class="header-title title is-4">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</h4>
                    </header>
                    <div class="content-detail">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018...
                      <ul class="c5-meta-data">
                        <li class="c5-meta-li-data">
                          <span>
                            <i class="fa fa-calendar"></i>
                            jan - 10 - 2018 - منذ ثلاث ساعات
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </article>
              </div>
              <div class="post-block-1 clearfix">
                <article class="post-1">
                  <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                    <div class="post-1-img">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" width="380px" height="150px" alt="">
                    </div>
                    <header class="content-header">
                      <!-- <h4 class="header-title title is-4">{{ $post->title }}</h4> -->
                      <h4 class="header-title title is-4">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</h4>
                    </header>
                    <div class="content-detail">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018...
                      <ul class="c5-meta-data">
                        <li class="c5-meta-li-data">
                          <span>
                            <i class="fa fa-calendar"></i>
                            jan - 10 - 2018 - منذ ثلاث ساعات
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </article>
              </div>
              <div class="post-block-1 clearfix">
                <article class="post-1">
                  <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                    <div class="post-1-img">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" width="380px" height="150px" alt="">
                    </div>
                    <header class="content-header">
                      <!-- <h4 class="header-title title is-4">{{ $post->title }}</h4> -->
                      <h4 class="header-title title is-4">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</h4>
                    </header>
                    <div class="content-detail">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018...
                      <ul class="c5-meta-data">
                        <li class="c5-meta-li-data">
                          <span>
                            <i class="fa fa-calendar"></i>
                            jan - 10 - 2018 - منذ ثلاث ساعات
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </article>
              </div>
              <div class="post-block-1 clearfix">
                <article class="post-1">
                  <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                    <div class="post-1-img">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" width="380px" height="150px" alt="">
                    </div>
                    <header class="content-header">
                      <!-- <h4 class="header-title title is-4">{{ $post->title }}</h4> -->
                      <h4 class="header-title title is-4">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</h4>
                    </header>
                    <div class="content-detail">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018...
                      <ul class="c5-meta-data">
                        <li class="c5-meta-li-data">
                          <span>
                            <i class="fa fa-calendar"></i>
                            jan - 10 - 2018 - منذ ثلاث ساعات
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </article>
              </div>
              <div class="post-block-1 clearfix">
                <article class="post-1">
                  <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                    <div class="post-1-img">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" width="380px" height="150px" alt="">
                    </div>
                    <header class="content-header">
                      <!-- <h4 class="header-title title is-4">{{ $post->title }}</h4> -->
                      <h4 class="header-title title is-4">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</h4>
                    </header>
                    <div class="content-detail">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018...
                      <ul class="c5-meta-data">
                        <li class="c5-meta-li-data">
                          <span>
                            <i class="fa fa-calendar"></i>
                            jan - 10 - 2018 - منذ ثلاث ساعات
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </article>
              </div>
              <div class="post-block-1 clearfix">
                <article class="post-1">
                  <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                    <div class="post-1-img">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" width="380px" height="150px" alt="">
                    </div>
                    <header class="content-header">
                      <!-- <h4 class="header-title title is-4">{{ $post->title }}</h4> -->
                      <h4 class="header-title title is-4">تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018</h4>
                    </header>
                    <div class="content-detail">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018...
                      <ul class="c5-meta-data">
                        <li class="c5-meta-li-data">
                          <span>
                            <i class="fa fa-calendar"></i>
                            jan - 10 - 2018 - منذ ثلاث ساعات
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </article>
              </div>
            <!-- @endforeach -->
          </div>
          <div class="c5ab-col-lg-4 col-lg-4 col-md-4 col-xs-12 col-sm-12">
            <hr class="star-short">
            <div class="c5ab-left-post">
              <!-- @foreach ($posts as $post) -->
                <article class="c5ab-post-author clearfix">
                  <h5 class="title is-5">
                    <a href="{{ route('site.post.show', ['slug' => $post->slug]) }}">
                      تغييرات وإضافات بسيطة لتحسين حياتك في عام 2018
                    </a>
                  </h5>
                  <!-- @if ($post->user->profile) -->
                    <a href="{{ url('/author', $post->user->name) }}" class="author">
                      <img src="{{ asset('images/posts/1513697100.jpg') }}" alt="" width="44" height="44">
                      البشير أحمد
                    </a>
                  <!-- @endif -->
                </article>
              <!-- @endforeach -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
